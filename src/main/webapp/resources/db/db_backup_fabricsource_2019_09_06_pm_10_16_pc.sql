-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: fabricsource
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(4000) DEFAULT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `sent_on` timestamp NULL DEFAULT NULL,
  `sent_by_name` tinytext,
  `cookie_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,'\"hi\"',0,NULL,'Anonymous',NULL),(2,'\"hi lll\"',0,NULL,'Anonymous',NULL),(3,'\"hi\"',0,NULL,'Anonymous',NULL),(4,'\"erter\"',0,NULL,'Anonymous',NULL),(5,'\"erter\"',0,NULL,'Anonymous',NULL),(6,'\"erter\"',0,NULL,'Anonymous',NULL),(7,'\"erter\"',0,NULL,'Anonymous',NULL),(8,'\"erter\"',0,NULL,'Anonymous',NULL),(9,'\"erter\"',0,NULL,'Anonymous',NULL),(10,'\"erter\"',0,NULL,'Anonymous',NULL),(11,'\"erter\"',0,NULL,'Anonymous',NULL),(12,'\"erter\"',0,NULL,'Anonymous',NULL),(13,'\"erter\"',0,NULL,'Anonymous',NULL),(14,'\"erter\"',0,NULL,'Anonymous',NULL),(15,'\"erter\"',0,NULL,'Anonymous',NULL),(16,'\"erter\"',0,NULL,'Anonymous',NULL),(17,'\"erter\"',0,NULL,'Anonymous',NULL),(18,'\"erter\"',0,NULL,'Anonymous',NULL),(19,'\"erter\"',0,NULL,'Anonymous',NULL),(20,'\"erter\"',0,NULL,'Anonymous',NULL),(21,'\"werw werw\"',0,NULL,'Anonymous',NULL),(22,'\"we \"',0,NULL,'Anonymous',NULL),(23,'\"ert\"',0,NULL,'Anonymous',NULL),(24,'\"rt\"',0,NULL,'Anonymous',NULL),(25,'\"er rt er\"',0,NULL,'Anonymous',NULL),(26,'\"d r\"',0,NULL,'Anonymous',NULL),(27,'\"yo\"',0,NULL,'Anonymous',NULL),(28,'\"eter\"',0,NULL,'Anonymous',NULL),(29,'\"gg gg\\ngtwf\"',5,NULL,'Manager',NULL),(30,'\"yuiyuiy\"',0,NULL,'Anonymous',NULL),(31,'\"iou\"',0,NULL,'Anonymous',NULL),(32,'\"fasdf afsdf\"',0,NULL,'Anonymous',NULL),(33,'\"erwe\"',0,NULL,'Anonymous',NULL),(34,'\"we we\"',0,NULL,'Anonymous',NULL),(35,'\"asdfs we\"',0,NULL,'Anonymous',NULL),(36,'\"ert \"',0,NULL,'Anonymous',NULL),(37,'\"',0,NULL,'Anonymous',NULL),(38,'f\"',0,NULL,'Anonymous',NULL),(39,'',0,NULL,'Anonymous',NULL),(40,'\"',0,NULL,'Anonymous',NULL),(41,'\"',0,NULL,'Anonymous',NULL),(42,'hyu',0,NULL,'Anonymous',NULL),(43,'',0,NULL,'Anonymous',NULL),(44,'kita be',0,NULL,'Anonymous',NULL),(45,'',0,NULL,'Anonymous',NULL),(46,'e',0,NULL,'Anonymous',NULL),(47,'oy',0,NULL,'Anonymous',NULL),(48,'i',0,NULL,'Anonymous',NULL),(49,'\\\"',0,NULL,'Anonymous',NULL),(50,'\'\'',0,NULL,'Anonymous',NULL),(51,'lol',0,NULL,'Anonymous',NULL),(59,'asd asdas',0,NULL,'Anonymous',NULL),(60,'afas',0,NULL,'Anonymous',NULL),(61,'eeee',0,NULL,'Anonymous',NULL),(62,'wer ewrt',2,NULL,'Md Al Amin',NULL),(63,'hi',0,NULL,'Anonymous',NULL),(64,'tttttttttt tttttttt',0,NULL,'Anonymous',NULL),(65,'hi',0,NULL,'Anonymous',45),(66,'hi',0,NULL,'Anonymous',47),(67,'htftyd',0,NULL,'Anonymous',47),(68,'wer',0,NULL,'Anonymous',48);
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_recipient`
--

DROP TABLE IF EXISTS `chat_recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chat_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `recipient_cookie_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_recipient_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_recipient`
--

LOCK TABLES `chat_recipient` WRITE;
/*!40000 ALTER TABLE `chat_recipient` DISABLE KEYS */;
INSERT INTO `chat_recipient` VALUES (1,59,1,NULL),(2,59,2,NULL),(3,59,3,NULL),(4,59,4,NULL),(5,59,5,NULL),(7,60,2,NULL),(8,60,3,NULL),(9,61,2,NULL),(10,61,4,NULL),(11,62,1,NULL),(12,62,2,NULL),(13,62,3,NULL),(14,62,4,NULL),(15,62,5,NULL),(16,62,6,NULL),(17,63,1,NULL),(18,63,2,NULL),(19,63,3,NULL),(20,63,4,NULL),(21,63,5,NULL),(22,63,6,NULL),(23,64,7,NULL),(24,65,7,0),(25,65,0,45),(26,66,7,0),(27,66,0,47),(28,67,7,0),(29,67,0,47),(30,68,7,0),(31,68,0,48);
/*!40000 ALTER TABLE `chat_recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `type` int(11) DEFAULT NULL,
  `link` varchar(1010) DEFAULT NULL,
  `details` varchar(4000) DEFAULT NULL,
  `image_link` varchar(1010) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (4,'WE\'VE BEEN SILENCED TOO LONG!',2,NULL,'Governor Brown is negligent to address the homelessness issues, the education issues and the employment issues, which are all tied together. Proper direction and management of our educational system to prevent homelessness, needs to happen!\r\n\r\n\r\n\r\n Oregon instead holds one of the lowest graduation rates in the nation.  We need to address the issue at its core, rather than ignoring Oregonians that are down on their luck.\r\n\r\n\r\n\r\nThe MANDATORY Vaccination is Draconian in nature and leaves us FLABBERGASTED that it\'s even being considered!!\r\n\r\n\r\n\r\nThe sexual abuse in the STATE FOSTER CARE SYSTEM IS RAMPANT!\r\n\r\n\r\n\r\nGov. Brown is complicit in the sexual atrocities our MOST VULNERABLE CHILDREN are BEING VICTIMIZED TO!\r\n\r\n\r\n\r\nTHE MADNESS NEEDS TO STOP!  \r\n\r\n\r\n\r\nWE MUST STOP KATE BROWN, before MORE children, mentally ill, and our wonderful senior citizens are VICTIMIZED, EUTHANIZED, STARVED OR SEXUALLY ASSAULTED!\r\n\r\n\r\n\r\nWE Have NO CHOICE!!  We Must STOP BROWN!\r\n\r\n\r\n\r\nWE NEED YOUR HELP! Please click the \"join our movement now\" button above and enter in the information!  Ask 3-10 people to do the same!  That\'s all there is to it!\r\n\r\n\r\n\r\nJOIN US ON FACEBOOK!  See the Link above!\r\n\r\n\r\n\r\nWE OWE IT TO OUR MOST VULNERABLE TO ACCOMPLISH THIS BEFORE IT IS TOO LATE!',NULL,5,'2019-07-20 22:07:54'),(5,'UNITED BY A COMMON GOAL',2,NULL,'We are compassionate Oregonians.  Compassion should be on the forefront of any leader\'s mind, however, our least advantaged persons (homeless population, mentally ill, senior citizens, foster children) are treated like inmates in correctional institutions or are simply discarded!\r\n\r\n\r\n\r\nWe need a new leader immediately that understands the challenges facing Oregon right now, which are unique to our beautiful state!',NULL,5,'2019-07-20 22:10:36'),(6,'GET INVOLVED',2,NULL,'This truly is a non partisan issue.  In fact, it\'s a COMMON SENSE kind of issue!\r\n\r\n\r\n\r\nDo you have children?  Are you CONCERNED about the FUTURE of our great state?\r\n\r\n\r\n\r\nSO MANY ISSUES, either being MISMANAGED, POORLY DIRECTED or NOT BEING ADDRESSED AT ALL.\r\n\r\n\r\n\r\nWon\'t you join us? Recall Kate Brown\r\n\r\nWHAT\'S THE PROCESS? \r\n\r\nWe need a sufficient BASE to collect 4,500 signatures per day, in 90 days or 400,000 signatures in total.  This takes into account the ones that are not valid.\r\nRight NOW, we NEED to build that base!  We are doing GREAT and on TRACK, but we NEED YOUR HELP!\r\nWe are asking people like YOU to ENROLL 3-10 people into this ALL IMPORTANT cause! After those folks do the same, we will have the 40,000 volunteers necessary to successfully accomplish this ambitious, but ESSENTIAL goal.\r\nPlease click the \"join our movement now\" button above and enter in the info needed.  Ask 3-10 people to do the same!  That\'s ALL there is to it right now!\r\nCheck us out on Facebook (see the link). You can have many of your questions answered there!  You can also download forms for the collection of volunteers OFF the website!\r\nWELCOME ABOARD!',NULL,5,'2019-07-20 22:10:36'),(7,'PART 1 Oregon First update',1,'2O2Id_6rxeQ',NULL,NULL,5,'2019-07-20 22:13:21'),(8,'PART 2 Oregon First update',1,'xOWhWaJgqys',NULL,NULL,5,'2019-07-20 22:13:21');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(1010) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_uindex` (`email`),
  UNIQUE KEY `newsletter_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
INSERT INTO `newsletter` VALUES (1,'alamincsesust@gmail.com');
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `our_cookie`
--

DROP TABLE IF EXISTS `our_cookie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `our_cookie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `our_cookie_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `our_cookie`
--

LOCK TABLES `our_cookie` WRITE;
/*!40000 ALTER TABLE `our_cookie` DISABLE KEYS */;
INSERT INTO `our_cookie` VALUES (45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58);
/*!40000 ALTER TABLE `our_cookie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) DEFAULT NULL,
  `current_role` int(11) DEFAULT NULL,
  `requested_role` int(11) DEFAULT NULL,
  `accepted_by` int(11) DEFAULT NULL,
  `rejected_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `request_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request`
--

LOCK TABLES `request` WRITE;
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
INSERT INTO `request` VALUES (1,'md_alamin3@yahoo.co.uk',99,1,NULL,NULL),(2,'md_alamin323@yahoo.co.uk',99,2,5,NULL);
/*!40000 ALTER TABLE `request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1010) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `mobile` tinytext,
  `email` tinytext,
  `password` tinytext,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Md Al Amin','Salimullah Road (Block-D), Mohammadpur','01711389230','md_alamin53@yahoo.co.uk','123',99),(2,'Md Al Amin','Salimullah Road (Block-D), Mohammadpur','01711389230','alamin@sparkyai.com','123',99),(3,'Md Al Amin','Salimullah Road (Block-D), Mohammadpur, 5/5','1711389230','md_alamin92@gmail.com','11',99),(4,'Md Al Amin','Salimullah Road (Block-D), Mohammadpur, 5/5','1711389230','md_alamin3@yahoo.co.uk','11',99),(5,'Manager','USA',NULL,'manager@campaign.com','11',1),(6,'Md Al Amin','Salimullah Road (Block-D), Mohammadpur, 5/5','1711389230','md_alamin323@yahoo.co.uk','123',2),(7,'Support',NULL,NULL,'support@fabricsource.com','11',3);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-06 22:16:52
