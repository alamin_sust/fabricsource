<%--
    Document   : home
    Created on : Aug 10, 2019, 3:16:38 PM
    Author     : Al-Amin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <!-- PAGE TITLE -->
    <title>FabricSource</title>

    <!-- META-DATA -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" >
    <meta name="keywords" content="" >

    <!-- FAVICON -->
    <link rel="shortcut icon" href="resources/assets/images/favicon.png">

    <!-- CSS:: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Caveat:400,700" rel="stylesheet">

    <!-- CSS:: OWL -->
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/owl/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/owl/owl.theme.default.min.css">

    <!-- CSS:: REVOLUTION -->
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/revolution/navigation.css">

    <!-- CSS:: MAIN -->
    <link rel="stylesheet" type="text/css" href="resources/assets/css/main.css">

    <link rel="stylesheet" href="resources/assets/css/chat.css">



    <script type="text/javascript">

        var arr = [];

        arr[0]= new Image();
        arr[0].src = "resources/assets/images/sl1.jpg";

        arr[1]= new Image();
        arr[1].src = "resources/assets/images/sl2.jpg";

        arr[2]= new Image();
        arr[2].src = "resources/assets/images/sl3.jpg";

        var i=0;

        function slide(){
            document.getElementById("image1").src= arr[i].src;
            i++;
            if(i==arr.length){
                i=0;
            }
            setTimeout(function(){ slide(); },5000);
        }

    </script>

</head>
<body onload="slide('image1',arr);">


<div class="b-main_menu-wrapper hidden-lg-up">
    <ul class="categories">
        <li class="has-sub dropdown-wrapper from-bottom">

        </li>
    </ul>
</div>
<div class="b-wrapper">
    <header id="b-header">
        <div class="b-header b-header_bg" style="background-color: #A8A8A8"
        <div class="container">
            <div class="b-header_topbar row clearfix">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="b-top_bar_left float-md-left text-center text-white">
                        <i class="fa fa-phone-square text-white mr-1"> </i>
                        OUR PHONE NUMBER:
                        <a href="tel:+972542424884" class="" style="color: black; text-decoration: none">+972542424884</a>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="b-header b-header_main">
    <div class="container">
        <div class="clearfix row">
            <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-12 col-xs-12 hidden-sm-down hidden-md-down">
                <div class="b-header_nav">
                    <div class="b-menu_top_bar_container">
                        <div class="b-main_menu menu-stay-left">
                            <ul class="categories pl-0 mb-0 list-unstyled">
                                <!-- Mega menu -->
                                <!-- Top level items -->

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-4 col-xs-6">
                <div class="b-logo text-sm-left text-lg-center text-xl-center">
                    <a href="home" class="d-inline-block"><img src="resources/assets/images/fabriclogo.png" class="img-fluid d-block" alt="" style="border-radius: 50%"></a>
                </div>
            </div>
        </div>
    </div>
</div>
</header>
<div  class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery">
    <div id="b-home_01_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
        <ul>
            <li data-index="rs-30" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb=""  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <img src="resources/assets/images/ban1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <div class="tp-caption tp-resizeme font-title rs-parallaxlevel-1"
                     data-x="['left','left','left','center']"
                     data-hoffset="['50','20','20','0']"
                     data-y="['middle','middle','middle','middle']"
                     data-voffset="['-47','-47','-47','-22']"
                     data-fontsize="['250','250','250','210']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":650,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"
                     style="font-size: 250px; font-family: lora; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 0.2); z-index: 999">2019 </div>
                <div class="tp-caption  tp-resizeme"
                     data-x="['left','left','left','center']"
                     data-hoffset="['50','20','20','0']"
                     data-y="['middle','middle','middle','middle']"
                     data-voffset="['-30','-30','-30','-24']"
                     data-fontsize="['120','120','120','70']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":500,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"
                     style="font-size: 120px; line-height: 22px; font-weight: 700; color: rgb(255, 255, 255); z-index: 999">
                    FabricSource
                </div>
                <div class="tp-caption tp-resizeme"
                     data-x="['left','left','left','center']"
                     data-hoffset="['50','20','20','0']"
                     data-y="['top','top','top','top']"
                     data-voffset="['350','318','318','230']"
                     data-fontsize="['73','73','73','40']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textalign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"
                     style="color: #fff;  z-index: 999; font-family: sans-serif;"><h1>We Mean Quality Products</h1>
                </div>

                <div class="tp-caption tp-resizeme"
                     data-x="['left','left','left','center']"
                     data-hoffset="['50','30','30','0']"
                     data-y="['top','top','top','top']"
                     data-voffset="['420','420','390','270']"
                     data-fontsize="['73','73','73','40']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textalign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]" >
                    <a href="#" class="btn btn-full" style="border-radius: 6px; background-color: #C06F37; border-color: #C06F37; margin-top: 18px;">LEARN MORE</a>
                </div>
            </li>
            <li data-index="rs-3045" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb=""  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <img src="resources/assets/images/ban2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                <div class="tp-caption tp-resizeme font-title rs-parallaxlevel-1"
                     data-x="['center','center','center','center']"
                     data-hoffset="['1','1','1','0']"
                     data-y="['middle','middle','middle','middle']"
                     data-voffset="['-47','-47','-47','-22']"
                     data-fontsize="['250','250','250','210']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":650,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"
                     style="background-color: #38393A; font-size: 250px; font-family: lora; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 0.2);">2019 </div>
                <div class="tp-caption  tp-resizeme"
                     data-x="['center','center','center','center']"
                     data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']"
                     data-voffset="['-30','-30','-30','-44']"
                     data-fontsize="['120','120','120','70']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":500,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"
                     style="background-color: #38393A; font-size: 120px; line-height: 22px; font-weight: 700; color: rgb(255, 255, 255);">
                    <i class="menu-tag new">NEW</i> STYLE
                </div>
                <div class="tp-caption tp-resizeme"
                     data-x="['left','center','center','center']"
                     data-hoffset="['300','0','0','-2']"
                     data-y="['top','top','top','top']"
                     data-voffset="['350','318','350','200']"
                     data-fontsize="['73','73','73','40']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-type="text"
                     data-responsive_offset="on"
                     data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textalign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"
                     style="font-family: sans-serif; color: #fff; font-weight: bold; text-align: center; margin-left: 120px"><h1 style="text-align:center;">For Men and Women</h1>
                </div>
                <div class="tp-caption NotGeneric-Icon   tp-resizeme"
                     id="slide-3045-layer-8"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7; white-space: nowrap;text-transform:left;cursor:default;"><i class="pe-7s-refresh"></i> </div>
            </li>



        </ul>
        <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>
    </div>
</div>
<section id="b-blog">
    <div class="b-section_title">
        <!--  <h4 class="text-center text-uppercase">
             Our Products
             <span class="b-title_separator"><span></span></span>
         </h4> -->
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Trending <b style="color: #9B5A2D">Products</b></h2>
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
                    <!-- Carousel indicators -->
                    <ol class="carousel-indicators" style="">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for carousel items -->
                    <div class="carousel-inner">
                        <div class="item carousel-item active">
                            <div class="row" style="">
                                <div class="col-sm-3" style="text-align: center;">
                                    <div class="thumb-wrapper" style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/imgpsh_fullsize_anim.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Under Wears</h4>
                                            <span>Our circular knit wears are under wears, t shirts, graphics t shirts, plain bulk t shirts, polo shirt, tank tops, rugby polo shirts, sweatshirts, pants, hoodies, cut and sewn t shirts, yarn dyed TC polo shirt, ladies elastane dress, fashion wears, design t shirts, night wear, bespoke leisurewear, jogging suits, track pants, tracksuits, pajama sets, ladies underwear &amp; panties, mens brief, boxer, team-wear, jerseys, sportswear, outerwear, active-wear, gym-wear, promotional clothing, custom print t shirts, fleece tops, fleece pullover, embroidered polo shirts, fancy knitted garments, knit denim pants, camisoles, Night Shirts, Panties, legging, bottoms, swimwear, beach wear, Mens underwear, zip hoodie, active wear, etc.</span>
                                            <p class="item-price"><strike>$400.00</strike> <span>$369.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper" style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C2.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Woven Wears</h4>
                                            <span>Our Woven wears are denim jeans, twill, TC pants, trousers, shorts, bermuda pants, BDU shorts, camo pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim pants, denim jackets, dress shirts, mens shirts, ski pants, snow suit, seasons jacket, fishing shirt, vest, golf pant, hunting jackets, hunting vest, hunting trouser, PU raincoat, waterproof jackets, windbreaker, sleepwear, dress shirts, blouse, skirts, chinos, school trousers, formal pants, joggers, casual jackets, uniforms, beachwear.</span>
                                            <p class="item-price"><strike>$25.00</strike> <span>$23.99</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper" style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C3.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Sweater</h4>
                                            <span>Our flat knitting products are Raglan pullover, Bow neck sweater, V Neck sweater, Ribber High-Low sweater, Stripe sweater, crew neck sweater, printed sweater, Shoulder patch pullover, open-knit sweater, tube yarn sweater, open cardigans, Ultra soft cardigan, jersey sweater, rib sweater, muffler, jumper, school uniform sweater, police uniform pullover etc.</span>
                                            <p class="item-price"><strike>$899.00</strike> <span>$649.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper" style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C4.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Workwear & Uniform</h4>
                                            <span>Our Woven wears are denim jeans, twill, cvc pants, trousers, shorts, bermuda pants, hi vis vest, hi viz jackets, camo pants, multi pocket pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim work pants, polar fleece jackets, fleece pants, work shirts, mens work shirts, overall, coverall, ski pants, fishing shirt, vest, fishing vest, PU raincoat, waterproof jackets, windbreaker, workwear, school trousers, boys pants, girls skirts, boys blazer, boys trouser, girls dress, uniforms, chef coats, kitchen apron, corporate clothing, corporate uniforms, hospitality uniforms, medical uniform, school uniforms, guard uniform, police uniform, reversible jackets.</span>
                                            <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item carousel-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/1.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Knit Wears</h4>
                                            <span>Our circular knit wears are t shirts, graphics t shirts, plain bulk t shirts, polo shirt, tank tops, rugby polo shirts, sweatshirts, pants, hoodies, cut and sewn t shirts, yarn dyed TC polo shirt, ladies elastane dress, fashion wears, design t shirts, night wear, bespoke leisurewear, jogging suits, track pants, tracksuits, pajama sets, ladies underwear &amp; panties, mens brief, boxer, team-wear, jerseys, sportswear, outerwear, active-wear, gym-wear, promotional clothing, custom print t shirts, fleece tops, fleece pullover, embroidered polo shirts, fancy knitted garments, knit denim pants, camisoles, Night Shirts, Panties, legging, bottoms, swimwear, beach wear, Mens underwear, zip hoodie, active wear, etc.</span>
                                            <p class="item-price"><strike>$289.00</strike> <span>$269.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/2.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Woven Wears</h4>
                                            <span>Our Woven wears are denim jeans, twill, TC pants, trousers, shorts, bermuda pants, BDU shorts, camo pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim pants, denim jackets, dress shirts, mens shirts, ski pants, snow suit, seasons jacket, fishing shirt, vest, golf pant, hunting jackets, hunting vest, hunting trouser, PU raincoat, waterproof jackets, windbreaker, sleepwear, dress shirts, blouse, skirts, chinos, school trousers, formal pants, joggers, casual jackets, uniforms, beachwear.</span>
                                            <p class="item-price"><strike>$1099.00</strike> <span>$869.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/3.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Sweater</h4>
                                            <span>Our flat knitting products are Raglan pullover, Bow neck sweater, V Neck sweater, Ribber High-Low sweater, Stripe sweater, crew neck sweater, printed sweater, Shoulder patch pullover, open-knit sweater, tube yarn sweater, open cardigans, Ultra soft cardigan, jersey sweater, rib sweater, muffler, jumper, school uniform sweater, police uniform pullover etc.</span>
                                            <p class="item-price"><strike>$109.00</strike> <span>$99.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/4.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Workwear & Uniform</h4>
                                            <span>Our Woven wears are denim jeans, twill, cvc pants, trousers, shorts, bermuda pants, hi vis vest, hi viz jackets, camo pants, multi pocket pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim work pants, polar fleece jackets, fleece pants, work shirts, mens work shirts, overall, coverall, ski pants, fishing shirt, vest, fishing vest, PU raincoat, waterproof jackets, windbreaker, workwear, school trousers, boys pants, girls skirts, boys blazer, boys trouser, girls dress, uniforms, chef coats, kitchen apron, corporate clothing, corporate uniforms, hospitality uniforms, medical uniform, school uniforms, guard uniform, police uniform, reversible jackets.</span>
                                            <p class="item-price"><strike>$599.00</strike> <span>$569.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item carousel-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C1.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Knit Wears</h4>
                                            <span>Our circular knit wears are t shirts, graphics t shirts, plain bulk t shirts, polo shirt, tank tops, rugby polo shirts, sweatshirts, pants, hoodies, cut and sewn t shirts, yarn dyed TC polo shirt, ladies elastane dress, fashion wears, design t shirts, night wear, bespoke leisurewear, jogging suits, track pants, tracksuits, pajama sets, ladies underwear &amp; panties, mens brief, boxer, team-wear, jerseys, sportswear, outerwear, active-wear, gym-wear, promotional clothing, custom print t shirts, fleece tops, fleece pullover, embroidered polo shirts, fancy knitted garments, knit denim pants, camisoles, Night Shirts, Panties, legging, bottoms, swimwear, beach wear, Mens underwear, zip hoodie, active wear, etc.</span>
                                            <p class="item-price"><strike>$369.00</strike> <span>$349.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C2.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Woven Wears</h4>
                                            <span>Our Woven wears are denim jeans, twill, TC pants, trousers, shorts, bermuda pants, BDU shorts, camo pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim pants, denim jackets, dress shirts, mens shirts, ski pants, snow suit, seasons jacket, fishing shirt, vest, golf pant, hunting jackets, hunting vest, hunting trouser, PU raincoat, waterproof jackets, windbreaker, sleepwear, dress shirts, blouse, skirts, chinos, school trousers, formal pants, joggers, casual jackets, uniforms, beachwear.</span>
                                            <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C3.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Sweater</h4>
                                            <span>Our flat knitting products are Raglan pullover, Bow neck sweater, V Neck sweater, Ribber High-Low sweater, Stripe sweater, crew neck sweater, printed sweater, Shoulder patch pullover, open-knit sweater, tube yarn sweater, open cardigans, Ultra soft cardigan, jersey sweater, rib sweater, muffler, jumper, school uniform sweater, police uniform pullover etc.</span>
                                            <p class="item-price"><strike>$450.00</strike> <span>$418.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="thumb-wrapper"  style="width: 220px">
                                        <div class="img-box">
                                            <img src="resources/assets/images/products/C4.jpg" class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <h4 style="font-weight: bold;">Workwear & Uniform</h4>
                                            <span>Our Woven wears are denim jeans, twill, cvc pants, trousers, shorts, bermuda pants, hi vis vest, hi viz jackets, camo pants, multi pocket pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim work pants, polar fleece jackets, fleece pants, work shirts, mens work shirts, overall, coverall, ski pants, fishing shirt, vest, fishing vest, PU raincoat, waterproof jackets, windbreaker, workwear, school trousers, boys pants, girls skirts, boys blazer, boys trouser, girls dress, uniforms, chef coats, kitchen apron, corporate clothing, corporate uniforms, hospitality uniforms, medical uniform, school uniforms, guard uniform, police uniform, reversible jackets.</span>
                                            <p class="item-price"><strike>$350.00</strike> <span>$330.00</span></p>
                                            <div class="star-rating">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                    <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Carousel controls -->
                    <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--         <div class="b-blog b-blog_grid text-center b-blog_grid_three mb-5">
                <div class="container">
                    <div class="row clearfix">
                        <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                            <div class="b-blog_grid_single">
                                <div class="b-blog_single_header">
                                    <div class="b-blog_img_wrap">
                                        <a href="#">
                                            <img src="resources/assets/images/products/C1.jpg" class="img-fluid d-block" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="b-blog_single_info">
                                    <h3 class="b-entry_title">
                                        <a href="#" rel="bookmark">Knit Wears</a>
                                    </h3>
                                    <div class="b-blog_single_content">
                                        <p>Our circular knit wears are t shirts, graphics t shirts, plain bulk t shirts, polo shirt, tank tops, rugby polo shirts, sweatshirts, pants, hoodies, cut and sewn t shirts, yarn dyed TC polo shirt, ladies elastane dress, fashion wears, design t shirts, night wear, bespoke leisurewear, jogging suits, track pants, tracksuits, pajama sets, ladies underwear &amp; panties, mens brief, boxer, team-wear, jerseys, sportswear, outerwear, active-wear, gym-wear, promotional clothing, custom print t shirts, fleece tops, fleece pullover, embroidered polo shirts, fancy knitted garments, knit denim pants, camisoles, Night Shirts, Panties, legging, bottoms, swimwear, beach wear, Mens underwear, zip hoodie, active wear, etc.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                            <div class="b-blog_grid_single">
                                <div class="b-blog_single_header">
                                    <div class="b-blog_img_wrap">
                                        <a href="#">
                                            <img src="resources/assets/images/products/C2.jpg" class="img-fluid d-block" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="b-blog_single_info">
                                    <h3 class="b-entry_title">
                                        <a href="#" rel="bookmark">Woven Wears</a>
                                    </h3>
                                    <div class="b-blog_single_content">
                                        <p>Our Woven wears are denim jeans, twill, TC pants, trousers, shorts, bermuda pants, BDU shorts, camo pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim pants, denim jackets, dress shirts, mens shirts, ski pants, snow suit, seasons jacket, fishing shirt, vest, golf pant, hunting jackets, hunting vest, hunting trouser, PU raincoat, waterproof jackets, windbreaker, sleepwear, dress shirts, blouse, skirts, chinos, school trousers, formal pants, joggers, casual jackets, uniforms, beachwear.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                            <div class="b-blog_grid_single">
                                <div class="b-blog_single_header">
                                    <div class="b-blog_img_wrap">
                                        <a href="#">
                                            <img src="resources/assets/images/products/C3.jpg" class="img-fluid d-block" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="b-blog_single_info">
                                    <h3 class="b-entry_title">
                                        <a href="#" rel="bookmark">Sweaters</a>
                                    </h3>
                                    <div class="b-blog_single_content">
                                        <p>Our flat knitting products are Raglan pullover, Bow neck sweater, V Neck sweater, Ribber High-Low sweater, Stripe sweater, crew neck sweater, printed sweater, Shoulder patch pullover, open-knit sweater, tube yarn sweater, open cardigans, Ultra soft cardigan, jersey sweater, rib sweater, muffler, jumper, school uniform sweater, police uniform pullover etc.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                            <div class="b-blog_grid_single">
                                <div class="b-blog_single_header">
                                    <div class="b-blog_img_wrap">
                                        <a href="#">
                                            <img src="resources/assets/images/products/C4.jpg" class="img-fluid d-block" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="b-blog_single_info">
                                    <h3 class="b-entry_title">
                                        <a href="#" rel="bookmark">Work Wear & Uniform</a>
                                    </h3>
                                    <div class="b-blog_single_content">
                                        <p>Our Woven wears are denim jeans, twill, cvc pants, trousers, shorts, bermuda pants, hi vis vest, hi viz jackets, camo pants, multi pocket pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim work pants, polar fleece jackets, fleece pants, work shirts, mens work shirts, overall, coverall, ski pants, fishing shirt, vest, fishing vest, PU raincoat, waterproof jackets, windbreaker, workwear, school trousers, boys pants, girls skirts, boys blazer, boys trouser, girls dress, uniforms, chef coats, kitchen apron, corporate clothing, corporate uniforms, hospitality uniforms, medical uniform, school uniforms, guard uniform, police uniform, reversible jackets.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div> -->
</section>


<section id="b-newsletter">
    <div class="b-newsletter b-newsletter_bg mb-5">
        <div class="b-newsletter_inner">
            <h3 class="text-center " style="font-family: sans-serif">Connect to FabricSource</h3>
            <h2 class="text-center" style="font-family: sans-serif">Join Our Newsletter</h2>
            <p class="text-center" style="font-family: sans-serif">Hey you, sign up it only takes a second to be the first to find out about our latest news and promotions…</p>
            <div class="b-newsletter_form">
                <form action="newsletter" class="clearfix">
                    <div class="form-group float-left">
                        <label>Email address: </label>
                        <input name="email" placeholder="Your email address" required="" type="email">
                    </div>
                    <div class="b-form_submit float-left">
                        <button class="b-submit">Sign up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<div id="b-gallery_logo_outer">
    <div class="b-gallery_logo">
        <div class="container">
            <div class="row clearfix">
                <div class="col-xl-12 col-lg-3 col-mb-3 col-sm-4 col-xs-12">
                    <h2 style="border: none !important;">our partners</h2>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-xl-12 col-lg-9 col-mb-9 col-sm-8 col-xs-12">
                    <div class="b-gallery_logo_list">
                        <ul class="p-0 m-0 owl-carousel owl-theme b-count_04" id="b-gallery_logo">
                            <li><a href="#"><img src="resources/assets/images/partners/1.jpg" class="img-fluid d-block" alt=""></a></li>
                            <li><a href="#"><img src="resources/assets/images/partners/2.jpg" class="img-fluid d-block" alt=""></a></li>
                            <li><a href="#"><img src="resources/assets/images/partners/3.jpg" class="img-fluid d-block" alt=""></a></li>
                            <li><a href="#"><img src="resources/assets/images/partners/4.jpg" class="img-fluid d-block" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-xl-12 col-lg-9 col-mb-9 col-sm-8 col-xs-12">
                    <div class="b-gallery_logo_list">
                        <ul class="p-0 m-0 owl-carousel owl-theme b-count_04" id="b-gallery_logo">
                            <li><a href="#"><img src="resources/assets/images/partners/5.jpg" class="img-fluid d-block" alt=""></a></li>
                            <li><a href="#"><img src="resources/assets/images/partners/6.jpg" class="img-fluid d-block" alt=""></a></li>
                            <li><a href="#"><img src="resources/assets/images/partners/7.jpg" class="img-fluid d-block" alt=""></a></li>
                            <li><a href="#"><img src="resources/assets/images/partners/8.jpg" class="img-fluid d-block" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--<div class="auto-slider" style="height: 700px">
    <ul class="auto-slider__content">
        <li><img src="resources/assets/images/sl2.jpg" ></li>
        <li><img src="resources/assets/images/sl3.jpg" ></li>
        <li><img src="resources/assets/images/sl1.jpg" ></li>
        <li><img src="resources/assets/images/sl2.jpg"></li>
    </ul>
</div>--%>

<div id="slidy-container" style="">
    <figure id="slidy" style="">
        <img src="resources/assets/images/sl2.jpg" >
        <img src="resources/assets/images/sl3.jpg" >
        <img src="resources/assets/images/sl1.jpg" >
        <img src="resources/assets/images/sl2.jpg" >
    </figure>
</div>

<!-- <div class="row">
    <img id="image1" style="width: 100%; height: auto;" src="" alt="slideshow">
</div> -->

<footer class="b-footer_container color-scheme-light hidden -sm-down">
    <div class="container b-main_footer">
        <!-- footer-main -->
        <aside class="row clearfix">

            <div class="b-footer_column col-lg-4 col-md-12 col-sm-12 mb-4">
                <div class="b-footer_block">
                    <h5 class="b-footer_block_title">About The Store</h5>
                    <div class="b-footer_block_in">
                        <p>STORE - worldwide fashion store since 1978. We sell over 1000+ branded products on our web-site.</p>
                        <div class="b-contact_info">
                            <i class="fa fa-location-arrow d-inline-block"></i> Skype: casinotechsource@gmail.com
                            <br>
                            <i class="fa fa-mobile d-inline-block"></i> Phone: +972542424884
                            <br><br>
                            <%--<div class="mapouter"><div class="gmap_canvas"><iframe width="720" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=dhaka&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net"></a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:720px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:720px;}</style></div>--%>
                            <!--  <div class="googlemap-responsive" style=" width: 100%; height: auto">
                              <iframe src="https://maps.google.com/maps?q=dhaka&t=&z=15&ie=UTF8&iwloc=&output=embed" width='720' height='500' frameborder='0' style="width: 90%; margin: 20px; box-sizing: border-box; justify-content: space-around;" ></iframe>

                           </div> -->

                            <!--  <div class="ggl" style="background-color: green;  width: 100%; height: auto; ">
                                  <div class="box" style="width: 30%; height: 300px;  background: yellow; margin: 20px; box-sizing: border-box; justify-content: space-around;"></div>
                              </div> -->
                            <!--  <style type="text/css">
                                 .ggl{
                                   width: 100%;
                                   height: auto;
                                   background: #007bbf;
                                 }
                                 .box{
                                   width: 20%
                                   height: 300px;
                                   background: yellow;
                                   margin: 20px;
                                   box-sizing: border-box;
                                 }
                            </style> -->

                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </aside>
    <div class="mapWrapper" style="width: 80%; height: 450px; position: relative; margin: auto; margin-left: 5px">
        <div class="overlay" onClick="style.pointerEvents='none'"></div>
        <iframe src="https://maps.google.com/maps?q=dhaka&t=&z=15&ie=UTF8&iwloc=&output=embed" width="" height="" frameborder="0" style="border:0; width: 100%; height: 450px;"></iframe>
    </div> <!-- end .mapWrapper-->
        <!-- footer-main -->
    </div>
    <!-- footer-bar -->
    <div class="b-copyrights_wrapper">
        <div class="container">
            <div class="d-footer_bar">
                <div class="text-center">
                    <i class="fa fa-copyright"></i> 2018 Created by
                    <a href="#" class="text-white">
                        jThemes Studio
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bar -->
</footer>
<a href="javascript:;" id="b-scrollToTop" class="b-scrollToTop" style="background-color: #9B5A2D; border-color: #9B5A2D">
    <span class="basel-tooltip-label">Scroll To Top</span>Scroll To Top
</a>
<div class="b-search_popup">
    <form role="search" method="get" id="searchform" class="searchform  basel-ajax-search" action="#" data-thumbnail="1" data-price="1" data-count="3">
        <div>
            <label class="screen-reader-text" for="s"></label>
            <input type="text" placeholder="Search for products" value="" name="s" id="s" autocomplete="off">
            <input type="hidden" name="post_type" id="post_type" value="product">
            <button type="submit" class="b-searchsubmit" id="b-searchsubmit">Search</button>
        </div>
    </form>
    <span class="b-close_search" id="b-close_search">close</span>
</div>
</div>
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade product_view" id="b-qucik_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-close btn-secondary" data-dismiss="modal">
                <i class="icon-close icons"></i>
            </button>
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-md-6 product_img">
                        <div class="owl-carousel owl-theme" id="b-product_pop_slider">
                            <div><img src="resources/assets/images/accessories-01.jpg" class="img-fluid d-block m-auto"></div>
                            <div><img src="resources/assets/images/accessories-02.jpg" class="img-fluid d-block m-auto"></div>
                        </div>
                    </div>
                    <div class="col-md-6 product_content pr-5 pt-4">
                        <div class="b-product_single_summary">
                            <h1>Jhecked Bag</h1>
                            <p class="b-price">
                              <span class="b-amount">
                              <span class="b-symbol">£</span>79.00</span>
                            </p>
                            <div class="b-produt_description">
                                <p>Adipiscing vehicula amet in natoque lobortis mus velit dis vestibulum ullamcorper senectus conubia suspendisse vestibulum nam condimentum aliquet ipsum justo eu vestibulum sagittis.A vel vehicula a mi varius porta.</p>
                            </div>
                            <div class="b-product_attr">
                                <div class="b-product_attr_single">
                                    <ul class="pl-0 list-unstyled clearfix">
                                        <li><span class="b-product_attr_title pt-1">Color:</span></li>
                                        <li><a href="#"><span data-toggle="tooltip" title="" data-original-title="Black" class="b-color_attr b-black"></span></a></li>
                                        <li><a href="#"><span data-toggle="tooltip" title="" data-original-title="Red" class="b-color_attr b-red"></span></a></li>
                                        <li><a href="#"><span data-toggle="tooltip" title="" data-original-title="Yellow" class="b-color_attr b-yellow"></span></a></li>
                                    </ul>
                                </div>
                                <div class="b-product_attr_single">
                                    <ul class="pl-0 list-unstyled clearfix">
                                        <li><span class="b-product_attr_title">Size:</span></li>
                                        <li><a href="#"><span class="b-size_attr">L</span></a></li>
                                        <li><a href="#"><span class="b-size_attr">XL</span></a></li>
                                        <li><a href="#"><span class="b-size_attr">XXL</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="b-product_single_action clearfix">
                                <div class="b-quantity pull-left">
                                    <input type="button" value="-" class="b-minus">
                                    <input type="text" step="1" min="1" max="" name="b-quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
                                    <input type="button" value="+" class="b-plus">
                                </div>
                                <button class="text-uppercase pull-left btn">add to cart</button>
                            </div>
                            <div class="b-product_single_option">
                                <ul class="pl-0 list-unstyled">
                                    <li><b class="text-uppercase">Sku</b>: N/A</li>
                                    <li><b>Category</b>: <a href="#">Man</a></li>
                                    <li>
                                        <b>Share</b>:
                                        <span class="b-share_product">
                                    <a href="#" class="fa fa-facebook"></a>
                                    <a href="#" class="fa fa-twitter"></a>
                                    <a href="#" class="fa fa-instagram"></a>
                                    <a href="#" class="fa fa-envelope"></a>
                                    <a href="#" class="fa fa-google-plus"></a>
                                    <a href="#" class="fa fa-pint"></a>
                                  </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal b-promo_popup" id="b-promo_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-close btn-secondary" data-dismiss="modal">
                <i class="icon-close icons"></i>
            </button>
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-md-12 b-promo_content">
                        <div class="b-product_single_summary">
                            <div><img src="resources/assets/images/fabriclogo.png" style="height:100px; width:150px; border-radius: 50%" class="img-fluid d-block m-auto"></div>
                            <h1>HEY YOU, SIGN UP AND CONNECT TO <b style="color: black; font-weight:bold; font-size: 40px">FabricSource</b></h1>
                            <p class="b-promo_text text-center">
                                Be the first to learn about our latest trends and get exclusive offers.
                            </p>
                            <div class="b-newsletter_form">
                                <form action="newsletter" class="clearfix">
                                    <div class="form-group clearfix">
                                        <label>Email address: </label>
                                        <input name="email" placeholder="Your email address" required="" type="email">
                                    </div>
                                    <div class="b-form_submit text-center">
                                        <button class="b-submit">Sign up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--     <button class="open-button" onclick="openForm()" style="background-color: #9B5A2D">chat with us&nbsp;&nbsp;<span><i class="fa fa-comment"></i></span></button> -->
<button class="open-button" onclick="openForm()" style="background-color: #9B5A2D; height: 30px;width: 180px;">
    <div class="icon" style="width: 90px; margin-left: -52px; margin-top: -8px">
        <i class="fa fa-envelope" style="margin-left: 2px"></i><span style="text-align: center; margin-right: -130px; padding: 10px ">Leave a message</span>
    </div>
</button>

<div class="chat-popup" id="myForm" style="border-radius: 9px; width: 300px">
    <form action="#" class="form-container" style="border-radius: 9px;">

        <label for="msg"><b><div id="result" style="overflow-y: scroll;  width: 380px;"></div></b></label>
        <div>
            <h6>Support<span style="float: right;"><a onclick="closeForm()"><i class="fa fa-close"></i></a></span></h6>
        </div>
        <p style="color: #898989">Sorry, we aren't online at the moment. Leave a message or give me your email ID.</p>
        <h7 style="color: #666666; font-weight: bold;">Introduce yourself*</h7>
        <input type="text" class="form-control" placeholder="Name or email" id="msg" name="msg" onkeydown="if (event.keyCode == 13) { sendMessage(); return false;}"/>
        <p style="color: #B0B0B0">Or Sign In With</p> <i class="fa fa-facebook-official" aria-hidden="true" style="color: #637BAD"></i> <!-- <img src="https://img.icons8.com/office/16/000000/facebook.png" style="color: #637BAD"> --> <img src="https://img.icons8.com/color/48/000000/google-logo.png" style="width: 15px;height: 15px"><!-- <i class="fa fa-google-plus-official" aria-hidden="true"></i> -->
        <h6 style="color: #787878">Message*</h6>
        <input style="height: 60px" type="text" class="form-control" placeholder="Type message.." id="msg" name="msg" onkeydown="if (event.keyCode == 13) { sendMessage(); return false;}"/>
        <br>
        <a href="#" class="btn btn-full" style="border-radius: 6px; background-color: #C06F37; border-color: #C06F37; height: 20px"><p style="position: relative; text-align: center; margin-top: -8px">Send</p></a>
        <br>
    </form>
</div>


<script type="text/javascript">
    function getMessages() {
        $.ajax({
            url : 'getMessages',
            success : function(data) {
                $('#result').html(data);
                var objDiv = document.getElementById("result");
                objDiv.scrollTop = objDiv.scrollHeight;
            }
        });
    }

    function sendMessageBefore() {
        if($("#msg").val() !== '' && $('#recipientList').val().length > 0) {
            $.post("sendMessage",
                {
                    message: JSON.stringify($("#msg").val()),
                    recipients: JSON.stringify($('#recipientList').val().join("-"))
                },
                function (data) {
                    $('#result').html(data);
                    $("#msg").val('');
                    var objDiv = document.getElementById("result");
                    objDiv.scrollTop = objDiv.scrollHeight;
                });
        }
    }

    function sendMessage() {
        if($("#msg").val() !== '') {
            $.post("sendMessage",
                {
                    message: JSON.stringify($("#msg").val()),
                    recipients: '0'
                },
                function (data) {
                    $('#result').html(data);
                    $("#msg").val('');
                    var objDiv = document.getElementById("result");
                    objDiv.scrollTop = objDiv.scrollHeight;
                });
        }
    }
</script>



<script type="text/javascript">
    var intervalId = 0;
    intervalId = setInterval(getMessages, 2000);
</script>

      <%--<script>
          // Material Select Initialization
          $(document).ready(function() {
              $('.mdb-select').materialSelect();
          });
      </script>--%>

<script>
    function sendMsg() {
        document.getElementById("myForm").style.display = "block";
    }

    function openForm() {
        document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
        document.getElementById("myForm").style.display = "none";
    }
</script>



<!-- JQUERY:: JQUERY.JS -->
<script src="resources/assets/js/jquery.min.js"></script>

<!-- JQUERY:: BOOTSTRAP.JS -->
<script src="resources/assets/js/tether.min.js"></script>
<script src="resources/assets/js/bootstrap.min.js"></script>

<!-- JQUERY:: OWL.JS -->
<script src="resources/assets/js/plugins/owl/owl.carousel.min.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="resources/assets/js/plugins/revolution/jquery.themepunch.tools.min.js"></script>
<script src="resources/assets/js/plugins/revolution/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
<script src="resources/assets/js/plugins/revolution/revolution.extension.layeranimation.min.js"></script>
<script src="resources/assets/js/plugins/revolution/revolution.extension.navigation.min.js"></script>
<script src="resources/assets/js/plugins/revolution/revolution.extension.parallax.min.js"></script>
<script src="resources/assets/js/plugins/revolution/revolution.extension.slideanims.min.js"></script>

<!-- JQUERY:: CUSTOM.JS -->
<script src="resources/assets/js/custom.js"></script>

</body>
</html>