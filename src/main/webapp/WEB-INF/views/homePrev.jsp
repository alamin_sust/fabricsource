<%--
    Document   : home
    Created on : Aug 10, 2019, 3:16:38 PM
    Author     : Al-Amin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <!-- PAGE TITLE -->
    <title>FabricSource</title>

    <!-- META-DATA -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" >
    <meta name="keywords" content="" >

    <!-- FAVICON -->
    <link rel="shortcut icon" href="resources/assets/images/favicon.png">

    <!-- CSS:: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Caveat:400,700" rel="stylesheet">

    <!-- CSS:: OWL -->
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/owl/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/owl/owl.theme.default.min.css">

    <!-- CSS:: REVOLUTION -->
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/css/plugins/revolution/navigation.css">

    <!-- CSS:: MAIN -->
    <link rel="stylesheet" type="text/css" href="resources/assets/css/main.css">

    <link rel="stylesheet" href="resources/assets/css/chat.css">

    <script type="text/javascript">

        var arr = [];

        arr[0]= new Image();
        arr[0].src = "resources/assets/images/sl1.jpg";

        arr[1]= new Image();
        arr[1].src = "resources/assets/images/sl2.jpg";

        arr[2]= new Image();
        arr[2].src = "resources/assets/images/sl3.jpg";

        var i=0;

        function slide(){
            document.getElementById("image1").src= arr[i].src;
            i++;
            if(i==arr.length){
                i=0;
            }
            setTimeout(function(){ slide(); },5000);
        }

    </script>
</head>
<body onload="slide('image1',arr);">


<div class="b-main_menu-wrapper hidden-lg-up">
    <ul class="categories">
        <li class="has-sub dropdown-wrapper from-bottom">
            <%--<a href="home"><span class="top">Home</span><i class="fa fa-angle-down"></i></a>--%>

        </li>
    </ul>
</div>
<div class="b-wrapper">
    <header id="b-header">
        <div class="b-header b-header_bg">
            <div class="container">
                <div class="b-header_topbar row clearfix">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="b-top_bar_left float-md-left text-center text-white">
                            <i class="fa fa-phone-square text-white mr-1"> </i>
                            OUR PHONE NUMBER:
                            <a href="tel:+77756334876" class="text-white ml-2">+77 (756) 334 876</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-header b-header_main">
            <div class="container">
                <div class="clearfix row">
                    <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-12 col-xs-12 hidden-sm-down hidden-md-down">
                        <div class="b-header_nav">
                            <div class="b-menu_top_bar_container">
                                <div class="b-main_menu menu-stay-left">
                                    <ul class="categories pl-0 mb-0 list-unstyled">
                                        <!-- Mega menu -->
                                        <!-- Top level items -->
                                        <%--<li class=" b-has_sub b-dropdown_wrapper from-bottom">
                                            <a href="home" class=" description ">
                                            <span class="top">Home</span></a>
                                            <!-- Sub Menu items -->
                                        </li>--%>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-4 col-xs-6">
                        <div class="b-logo text-sm-left text-lg-center text-xl-center">
                            <a href="home" class="d-inline-block"><img src="resources/assets/images/fabricsource-logo.png" class="img-fluid d-block" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div  class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery">
        <div id="b-home_01_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                <li data-index="rs-30" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="resources/assets/images/slider/home/main-big-baner-1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="resources/assets/images/ban1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption tp-resizeme font-title rs-parallaxlevel-1"
                         data-x="['left','left','left','center']"
                         data-hoffset="['50','20','20','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-47','-47','-47','-22']"
                         data-fontsize="['250','250','250','210']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":650,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="font-size: 250px; font-family: lora; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 0.2); z-index: 999">2019 </div>
                    <div class="tp-caption  tp-resizeme"
                         data-x="['left','left','left','center']"
                         data-hoffset="['50','20','20','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-30','-30','-30','-24']"
                         data-fontsize="['120','120','120','70']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":500,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="font-size: 120px; line-height: 22px; font-weight: 700; color: rgb(255, 255, 255); z-index: 999">
                        FabricSource
                    </div>
                    <div class="tp-caption tp-resizeme"
                         data-x="['left','left','left','center']"
                         data-hoffset="['50','20','20','0']"
                         data-y="['top','top','top','top']"
                         data-voffset="['350','318','318','230']"
                         data-fontsize="['73','73','73','40']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="font-family: Caveat; color: #fff; font-weight: bold; z-index: 999">We Mean Quality Products
                    </div>
                    <%--<div class="tp-caption tp-resizeme font-title rs-parallaxlevel-1"
                         data-x="['right','right','right','center']"
                         data-hoffset="['50','1','1','0']"
                         data-y="['middle','middle','middle','center']"
                         data-voffset="['-47','-47','-47','0']"
                         data-fontsize="['250','250','250','210']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":650,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">
                          <img src="resources/assets/images/6.jpg" alt="" data-ww="['480px','300px','300px','250px']" data-hh="['748px','460px','460px','390px']" data-no-retina="" style="transform: matrix3d(0.990268, 0.139173, 0, 0, -0.139173, 0.990268, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                     </div>--%>
                    <div class="tp-caption tp-resizeme"
                         data-x="['left','left','left','center']"
                         data-hoffset="['50','30','30','0']"
                         data-y="['top','top','top','top']"
                         data-voffset="['420','420','390','270']"
                         data-fontsize="['73','73','73','40']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]" >
                        <a href="#" class="btn btn-full" style="border-radius: 6px;">LEARN MORE</a>
                    </div>
                </li>
                <li data-index="rs-3045" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="resources/assets/images/slider/home/main-big-baner-2.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="resources/assets/images/ban2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                    <div class="tp-caption tp-resizeme font-title rs-parallaxlevel-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['1','1','1','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-47','-47','-47','-22']"
                         data-fontsize="['250','250','250','210']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":650,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="font-size: 250px; font-family: lora; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 0.2);">2019 </div>
                    <div class="tp-caption  tp-resizeme"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-30','-30','-30','-44']"
                         data-fontsize="['120','120','120','70']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":500,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="font-size: 120px; line-height: 22px; font-weight: 700; color: rgb(255, 255, 255);">
                        <i class="menu-tag new">NEW</i> STYLE
                    </div>
                    <div class="tp-caption tp-resizeme"
                         data-x="['left','center','center','center']"
                         data-hoffset="['300','0','0','-2']"
                         data-y="['top','top','top','top']"
                         data-voffset="['350','318','350','200']"
                         data-fontsize="['73','73','73','40']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="font-family: Caveat; color: #fff; font-weight: bold;">For Men and Women
                    </div>
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme"
                         id="slide-3045-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; white-space: nowrap;text-transform:left;cursor:default;"><i class="pe-7s-refresh"></i> </div>
                </li>



            </ul>
            <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>
        </div>
    </div>
    <section id="b-blog">
        <div class="b-section_title">
            <h4 class="text-center text-uppercase">
                Our Products
                <span class="b-title_separator"><span></span></span>
            </h4>
        </div>
        <div class="b-blog b-blog_grid text-center b-blog_grid_three mb-5">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                        <div class="b-blog_grid_single">
                            <div class="b-blog_single_header">
                                <div class="b-blog_img_wrap">
                                    <a href="#">
                                        <img src="resources/assets/images/products/C1.jpg" class="img-fluid d-block" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="b-blog_single_info">
                                <h3 class="b-entry_title">
                                    <a href="#" rel="bookmark">Knit Wears</a>
                                </h3>
                                <div class="b-blog_single_content">
                                    <p>Our circular knit wears are t shirts, graphics t shirts, plain bulk t shirts, polo shirt, tank tops, rugby polo shirts, sweatshirts, pants, hoodies, cut and sewn t shirts, yarn dyed TC polo shirt, ladies elastane dress, fashion wears, design t shirts, night wear, bespoke leisurewear, jogging suits, track pants, tracksuits, pajama sets, ladies underwear &amp; panties, mens brief, boxer, team-wear, jerseys, sportswear, outerwear, active-wear, gym-wear, promotional clothing, custom print t shirts, fleece tops, fleece pullover, embroidered polo shirts, fancy knitted garments, knit denim pants, camisoles, Night Shirts, Panties, legging, bottoms, swimwear, beach wear, Mens underwear, zip hoodie, active wear, etc.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                        <div class="b-blog_grid_single">
                            <div class="b-blog_single_header">
                                <div class="b-blog_img_wrap">
                                    <a href="#">
                                        <img src="resources/assets/images/products/C2.jpg" class="img-fluid d-block" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="b-blog_single_info">
                                <h3 class="b-entry_title">
                                    <a href="#" rel="bookmark">Woven Wears</a>
                                </h3>
                                <div class="b-blog_single_content">
                                    <p>Our Woven wears are denim jeans, twill, TC pants, trousers, shorts, bermuda pants, BDU shorts, camo pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim pants, denim jackets, dress shirts, mens shirts, ski pants, snow suit, seasons jacket, fishing shirt, vest, golf pant, hunting jackets, hunting vest, hunting trouser, PU raincoat, waterproof jackets, windbreaker, sleepwear, dress shirts, blouse, skirts, chinos, school trousers, formal pants, joggers, casual jackets, uniforms, beachwear.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                        <div class="b-blog_grid_single">
                            <div class="b-blog_single_header">
                                <div class="b-blog_img_wrap">
                                    <a href="#">
                                        <img src="resources/assets/images/products/C3.jpg" class="img-fluid d-block" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="b-blog_single_info">
                                <h3 class="b-entry_title">
                                    <a href="#" rel="bookmark">Sweaters</a>
                                </h3>
                                <div class="b-blog_single_content">
                                    <p>Our flat knitting products are Raglan pullover, Bow neck sweater, V Neck sweater, Ribber High-Low sweater, Stripe sweater, crew neck sweater, printed sweater, Shoulder patch pullover, open-knit sweater, tube yarn sweater, open cardigans, Ultra soft cardigan, jersey sweater, rib sweater, muffler, jumper, school uniform sweater, police uniform pullover etc.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-mb-4 col-sm-6 col-xs-12">
                        <div class="b-blog_grid_single">
                            <div class="b-blog_single_header">
                                <div class="b-blog_img_wrap">
                                    <a href="#">
                                        <img src="resources/assets/images/products/C4.jpg" class="img-fluid d-block" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="b-blog_single_info">
                                <h3 class="b-entry_title">
                                    <a href="#" rel="bookmark">Work Wear & Uniform</a>
                                </h3>
                                <div class="b-blog_single_content">
                                    <p>Our Woven wears are denim jeans, twill, cvc pants, trousers, shorts, bermuda pants, hi vis vest, hi viz jackets, camo pants, multi pocket pants, camouflage cargo pants, cargo shorts, swim shorts, capri, casual shirt, rainwear, formal shirts, denim work pants, polar fleece jackets, fleece pants, work shirts, mens work shirts, overall, coverall, ski pants, fishing shirt, vest, fishing vest, PU raincoat, waterproof jackets, windbreaker, workwear, school trousers, boys pants, girls skirts, boys blazer, boys trouser, girls dress, uniforms, chef coats, kitchen apron, corporate clothing, corporate uniforms, hospitality uniforms, medical uniform, school uniforms, guard uniform, police uniform, reversible jackets.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <%--<div class="b-load_more text-center">
                    <a href="#" class="btn b-blog_load_more">Load More Posts</a>
                </div>--%>
            </div>
        </div>
    </section>


    <section id="b-newsletter">
        <div class="b-newsletter b-newsletter_bg mb-5">
            <div class="b-newsletter_inner">
                <h3 class="text-center font-italic">Connect to FabricSource</h3>
                <h2 class="text-center">Join Our Newsletter</h2>
                <p class="text-center">Hey you, sign up it only takes a second to be the first to find out about our latest news and promotions…</p>
                <div class="b-newsletter_form">
                    <form action="newsletter" class="clearfix">
                        <div class="form-group float-left">
                            <label>Email address: </label>
                            <input name="email" placeholder="Your email address" required="" type="email">
                        </div>
                        <div class="b-form_submit float-left">
                            <button class="b-submit">Sign up</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <div id="b-gallery_logo_outer">
        <div class="b-gallery_logo">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-3 col-mb-3 col-sm-4 col-xs-12">
                        <h2 style="border: none !important;">our partners</h2>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-9 col-mb-9 col-sm-8 col-xs-12">
                        <div class="b-gallery_logo_list">
                            <ul class="p-0 m-0 owl-carousel owl-theme b-count_04" id="b-gallery_logo">
                                <li><a href="#"><img src="resources/assets/images/partners/1.jpg" class="img-fluid d-block" alt=""></a></li>
                                <li><a href="#"><img src="resources/assets/images/partners/2.jpg" class="img-fluid d-block" alt=""></a></li>
                                <li><a href="#"><img src="resources/assets/images/partners/3.jpg" class="img-fluid d-block" alt=""></a></li>
                                <li><a href="#"><img src="resources/assets/images/partners/4.jpg" class="img-fluid d-block" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-9 col-mb-9 col-sm-8 col-xs-12">
                        <div class="b-gallery_logo_list">
                            <ul class="p-0 m-0 owl-carousel owl-theme b-count_04" id="b-gallery_logo">
                                <li><a href="#"><img src="resources/assets/images/partners/5.jpg" class="img-fluid d-block" alt=""></a></li>
                                <li><a href="#"><img src="resources/assets/images/partners/6.jpg" class="img-fluid d-block" alt=""></a></li>
                                <li><a href="#"><img src="resources/assets/images/partners/7.jpg" class="img-fluid d-block" alt=""></a></li>
                                <li><a href="#"><img src="resources/assets/images/partners/8.jpg" class="img-fluid d-block" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <img id="image1" style="width: 100%; height: auto;" src="" alt="slideshow">
    </div>

    <footer class="b-footer_container color-scheme-light hidden -sm-down">
        <div class="container b-main_footer">
            <!-- footer-main -->
            <aside class="row clearfix">
                <%--<div class="b-footer_column col-md-12 col-sm-12">
                    <div class="b-footer_block">
                        <div class="b-footer_block_in">
                            <p class="text-center mb-0"><img id="image2" src="resources/assets/images/4.jpg" class="d-block m-auto img-fluid" alt="" title=""></p>
                            <ul class="b-social-icons text-center">
                                <li class="b-social_facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i>Facebook</a></li>
                                <li class="b-social_twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i>Twitter</a></li>
                                <li class="b-social_google"><a href="#" target="_blank"><i class="fa fa-google-plus"></i>Google</a></li>
                                <li class="b-social_email"><a href="#" target="_blank"><i class="fa fa-envelope"></i>Email</a></li>
                                <li class="b-social_pinterest"><a href="#" target="_blank"><i class="fa fa-pinterest"></i>Pinterest</a></li>
                            </ul>
                            <br>
                        </div>
                    </div>
                </div>--%>

                <div class="b-footer_column col-lg-4 col-md-12 col-sm-12 mb-4">
                    <div class="b-footer_block">
                        <h5 class="b-footer_block_title">About The Store</h5>
                        <div class="b-footer_block_in">
                            <p>STORE - worldwide fashion store since 1978. We sell over 1000+ branded products on our web-site.</p>
                            <div class="b-contact_info">
                                <i class="fa fa-location-arrow d-inline-block"></i> 451 Wall Street, USA, New York
                                <br>
                                <i class="fa fa-mobile d-inline-block"></i> Phone: (064) 332-1233
                                <br><br>
                                <div class="mapouter"><div class="gmap_canvas"><iframe width="720" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=dhaka&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net"></a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:720px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:720px;}</style></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- footer-main -->
        </div>
        <!-- footer-bar -->
        <div class="b-copyrights_wrapper">
            <div class="container">
                <div class="d-footer_bar">
                    <div class="text-center">
                        <i class="fa fa-copyright"></i> 2018 Created by
                        <a href="#" class="text-white">
                            jThemes Studio
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bar -->
    </footer>
    <a href="javascript:;" id="b-scrollToTop" class="b-scrollToTop">
        <span class="basel-tooltip-label">Scroll To Top</span>Scroll To Top
    </a>
    <div class="b-search_popup">
        <form role="search" method="get" id="searchform" class="searchform  basel-ajax-search" action="#" data-thumbnail="1" data-price="1" data-count="3">
            <div>
                <label class="screen-reader-text" for="s"></label>
                <input type="text" placeholder="Search for products" value="" name="s" id="s" autocomplete="off">
                <input type="hidden" name="post_type" id="post_type" value="product">
                <button type="submit" class="b-searchsubmit" id="b-searchsubmit">Search</button>
            </div>
        </form>
        <span class="b-close_search" id="b-close_search">close</span>
    </div>
</div>
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade product_view" id="b-qucik_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-close btn-secondary" data-dismiss="modal">
                <i class="icon-close icons"></i>
            </button>
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-md-6 product_img">
                        <div class="owl-carousel owl-theme" id="b-product_pop_slider">
                            <div><img src="resources/assets/images/accessories-01.jpg" class="img-fluid d-block m-auto"></div>
                            <div><img src="resources/assets/images/accessories-02.jpg" class="img-fluid d-block m-auto"></div>
                        </div>
                    </div>
                    <div class="col-md-6 product_content pr-5 pt-4">
                        <div class="b-product_single_summary">
                            <h1>Jhecked Bag</h1>
                            <p class="b-price">
                              <span class="b-amount">
                              <span class="b-symbol">£</span>79.00</span>
                            </p>
                            <div class="b-produt_description">
                                <p>Adipiscing vehicula amet in natoque lobortis mus velit dis vestibulum ullamcorper senectus conubia suspendisse vestibulum nam condimentum aliquet ipsum justo eu vestibulum sagittis.A vel vehicula a mi varius porta.</p>
                            </div>
                            <div class="b-product_attr">
                                <div class="b-product_attr_single">
                                    <ul class="pl-0 list-unstyled clearfix">
                                        <li><span class="b-product_attr_title pt-1">Color:</span></li>
                                        <li><a href="#"><span data-toggle="tooltip" title="" data-original-title="Black" class="b-color_attr b-black"></span></a></li>
                                        <li><a href="#"><span data-toggle="tooltip" title="" data-original-title="Red" class="b-color_attr b-red"></span></a></li>
                                        <li><a href="#"><span data-toggle="tooltip" title="" data-original-title="Yellow" class="b-color_attr b-yellow"></span></a></li>
                                    </ul>
                                </div>
                                <div class="b-product_attr_single">
                                    <ul class="pl-0 list-unstyled clearfix">
                                        <li><span class="b-product_attr_title">Size:</span></li>
                                        <li><a href="#"><span class="b-size_attr">L</span></a></li>
                                        <li><a href="#"><span class="b-size_attr">XL</span></a></li>
                                        <li><a href="#"><span class="b-size_attr">XXL</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="b-product_single_action clearfix">
                                <div class="b-quantity pull-left">
                                    <input type="button" value="-" class="b-minus">
                                    <input type="text" step="1" min="1" max="" name="b-quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
                                    <input type="button" value="+" class="b-plus">
                                </div>
                                <button class="text-uppercase pull-left btn">add to cart</button>
                            </div>
                            <div class="b-product_single_option">
                                <ul class="pl-0 list-unstyled">
                                    <li><b class="text-uppercase">Sku</b>: N/A</li>
                                    <li><b>Category</b>: <a href="#">Man</a></li>
                                    <li>
                                        <b>Share</b>:
                                        <span class="b-share_product">
                                    <a href="#" class="fa fa-facebook"></a>
                                    <a href="#" class="fa fa-twitter"></a>
                                    <a href="#" class="fa fa-instagram"></a>
                                    <a href="#" class="fa fa-envelope"></a>
                                    <a href="#" class="fa fa-google-plus"></a>
                                    <a href="#" class="fa fa-pint"></a>
                                  </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal b-promo_popup" id="b-promo_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-close btn-secondary" data-dismiss="modal">
                <i class="icon-close icons"></i>
            </button>
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-md-12 b-promo_content">
                        <div class="b-product_single_summary">
                            <div><img src="resources/assets/images/fabricsource-logo.png" class="img-fluid d-block m-auto"></div>
                            <h1>HEY YOU, SIGN UP AND CONNECT TO <b>FabricSource</b></h1>
                            <p class="b-promo_text text-center">
                                Be the first to learn about our latest trends and get exclusive offers.
                            </p>
                            <div class="b-newsletter_form">
                                <form action="newsletter" class="clearfix">
                                    <div class="form-group clearfix">
                                        <label>Email address: </label>
                                        <input name="email" placeholder="Your email address" required="" type="email">
                                    </div>
                                    <div class="b-form_submit text-center">
                                        <button class="b-submit">Sign up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<%--<c:if test="${sessionScope.get('user') != null}">--%>
<button class="open-button" onclick="openForm()">chat with us&nbsp;&nbsp;<span><i class="fa fa-comment"></i></span></button>
<%--</c:if>--%>

<div class="chat-popup" id="myForm" style="border-radius: 9px;">
    <form action="#" class="form-container" style="border-radius: 9px;">
        <h5>chat with us<span style="float: right;"><a onclick="closeForm()"><i class="fa fa-close"></i></a></span></h5>
        <%--<c:choose>
            <c:when test="${sessionScope.user != null}">
                <div class="row">
                    <div class="col-md-12">
                        <label class="mdb-main-label">Add Recipients</label>
                        <select class="form form-control" id="recipientList" name="recipientList" multiple searchable="Search here..">
                            <option value="0" selected>Broadcast (Everyone)</option>
                            <c:forEach items="${allowedRecipientMap}" var="recipient">
                                <option value="${recipient.value}">${recipient.key}</option>
                            </c:forEach>
                        </select>
                        <br>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row" style="visibility: hidden">
                    <div class="col-md-12">
                        <label class="mdb-main-label">Add Recipients</label>
                        <select class="form form-control" id="recipientList" name="recipientList" multiple searchable="Search here..">
                            <option value="0" selected>Broadcast (Everyone)</option>
                            <c:forEach items="${allowedRecipientMap}" var="recipient">
                                <option value="${recipient.value}">${recipient.key}</option>
                            </c:forEach>
                        </select>
                        <br>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>--%>

        <label for="msg"><b><div id="result" style="overflow-y: scroll; height: 300px; width: 380px;"></div></b></label>

        <input type="text" class="form-control" placeholder="Type message.." id="msg" name="msg" onkeydown="if (event.keyCode == 13) { sendMessage(); return false;}"/>
        <br>
        <%--<button type="button" class="btn" onclick="sendMessage()">Send</button>
        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>--%>
    </form>
</div>


<script type="text/javascript">
    function getMessages() {
        $.ajax({
            url : 'getMessages',
            success : function(data) {
                $('#result').html(data);
                var objDiv = document.getElementById("result");
                objDiv.scrollTop = objDiv.scrollHeight;
            }
        });
    }

    function sendMessageBefore() {
        if($("#msg").val() !== '' && $('#recipientList').val().length > 0) {
            $.post("sendMessage",
                {
                    message: JSON.stringify($("#msg").val()),
                    recipients: JSON.stringify($('#recipientList').val().join("-"))
                },
                function (data) {
                    $('#result').html(data);
                    $("#msg").val('');
                    var objDiv = document.getElementById("result");
                    objDiv.scrollTop = objDiv.scrollHeight;
                });
        }
    }

    function sendMessage() {
        if($("#msg").val() !== '') {
            $.post("sendMessage",
                {
                    message: JSON.stringify($("#msg").val()),
                    recipients: '0'
                },
                function (data) {
                    $('#result').html(data);
                    $("#msg").val('');
                    var objDiv = document.getElementById("result");
                    objDiv.scrollTop = objDiv.scrollHeight;
                });
        }
    }
</script>



<script type="text/javascript">
    var intervalId = 0;
    intervalId = setInterval(getMessages, 2000);
</script>

<%--<script>
    // Material Select Initialization
    $(document).ready(function() {
        $('.mdb-select').materialSelect();
    });
</script>--%>

<script>
    function sendMsg() {
        document.getElementById("myForm").style.display = "block";
    }

    function openForm() {
        document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
        document.getElementById("myForm").style.display = "none";
    }
</script>



<!-- JQUERY:: JQUERY.JS -->
<script src="resources/assets/js/jquery.min.js"></script>

<!-- JQUERY:: BOOTSTRAP.JS -->
<script src="resources/assets/js/tether.min.js"></script>
<script src="resources/assets/js/bootstrap.min.js"></script>

<!-- JQUERY:: OWL.JS -->
<script src="resources/assets/js/plugins/owl/owl.carousel.min.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="resources/assets/js/plugins/revolution/jquery.themepunch.tools.min.js"></script>
<script src="resources/assets/js/plugins/revolution/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
<script src="resources/assets/js/plugins/revolution/revolution.extension.layeranimation.min.js"></script>
<script src="resources/assets/js/plugins/revolution/revolution.extension.navigation.min.js"></script>
<script src="resources/assets/js/plugins/revolution/revolution.extension.parallax.min.js"></script>
<script src="resources/assets/js/plugins/revolution/revolution.extension.slideanims.min.js"></script>

<!-- JQUERY:: CUSTOM.JS -->
<script src="resources/assets/js/custom.js"></script>

</body>
</html>