<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-RU">
<head>
    <!-- PAGE TITLE -->
    <title>Basel My Account</title>

    <!-- META-DATA -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" >
    <meta name="keywords" content="" >

    <!-- FAVICON -->
    <link rel="shortcut icon" href="assets/images/favicon.png">

    <!-- CSS:: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <!-- CSS:: MAIN -->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
<body>
<div class="b-mini_cart">
    <div class="b-mini_cart_header">
        SHOPPING CART
        <span class="b-close_search" id="b-close_cart"></span>
    </div>
    <ul class="b-mini_cart_items mb-0 list-unstyled">
        <li class="clearfix">
            <img src="assets/images/products/home/product_grid_01_01.jpg" width="50" alt="item1">
            <span class="item-name">Sony DSC-RX100M III</span>
            <span class="item-price">1&nbsp;x&nbsp;<span>$849.99</span></span>
        </li>

        <li class="clearfix">
            <img src="assets/images/products/home/product_grid_02_01.jpg" width="50" alt="item1">
            <span class="item-name">KS Automatic Mechanic...</span>
            <span class="item-price">1&nbsp;x&nbsp;<span>$849.99</span></span>
        </li>

        <li class="clearfix">
            <img src="assets/images/products/home/product_grid_03_01.jpg" width="50" alt="item1">
            <span class="item-name">Kindle, 6" Glare-Free To...</span>
            <span class="item-price">1&nbsp;x&nbsp;<span>$849.99</span></span>
        </li>
    </ul>
    <div class="shopping-cart-total clearfix pl-3 pr-3 mb-4">
        <span class="lighter-text float-left">Total:</span>
        <span class="main-color-text float-right">$2,229.97</span>
    </div>
    <div class="pl-3 pr-3">
        <a href="cart-default.html" class="btn d-block mb-2">Cart</a>
        <a href="checkout.html" class="btn btn-bg d-block">Checkout</a>
    </div>
</div>
<div class="b-main_menu-wrapper hidden-lg-up">
    <ul class="mobile-top">
        <li class="search">
            <div class="search-holder-mobile">
                <input type="text" name="search-mobile" value="" placeholder="Search" class="form-control">
                <a class="fa fa-search"></a>
            </div>
        </li>
    </ul>
    <ul class="categories">
        <li class="has-sub dropdown-wrapper from-bottom">
            <a href="indexPrev.html"><span class="top">Home</span><i class="fa fa-angle-down"></i></a>
            <div class="dropdown-content sub-holder dropdown-left narrow">
                <div class="dropdown-inner">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-12 ">
                            <div class="menu-item">
                                <!-- CATEGORIES CONTENT -->
                                <div class="categories">
                                    <div class="clearfix">
                                        <div class="col-sm-12 hover-menu text-uppercase">
                                            <ul>
                                                <li><a href="indexPrev.html">HOME DEFAULT</a></li>
                                                <li><a href="home-lingerie.html">Lingerie store</a></li>
                                                <li><a href="home-watch.html">HOME Watches</a></li>
                                                <li><a href="home-minimalist.html">home minimalist</a></li>
                                                <li><a href="home-jewellery.html">home jewellery</a></li>
                                                <li><a href="home-furniture.html">home furniture</a></li>
                                                <li><a href="home-full-width.html">home fashion full</a></li>
                                                <li><a href="home-fashion.html">home fashion</a></li>
                                                <li><a href="home-cosmetics.html">home cosmetics</a></li>
                                                <li><a href="home-dark.html">home dark</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                </div>
            </div>
        </li>
        <!-- Top level items -->
        <li class=" has-sub dropdown-wrapper from-bottom">
            <a href="shop-grid-three.html"><span class="top">Shop</span><i class="fa fa-angle-down"></i></a>
            <!-- Sub Menu items -->
            <div class="dropdown-content sub-holder dropdown-left narrow">
                <div class="dropdown-inner">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-12 ">
                            <div class="menu-item">
                                <div class="categories">
                                    <div class="clearfix">
                                        <div class="col-sm-12 hover-menu">
                                            <ul>
                                                <li><a href="shop-grid-three.html">shop</a></li>
                                                <li><a href="product-single.html">single product</a></li>
                                                <li><a href="#">checkout</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <a href="blog-default.html"><span class="top">Blog</span></a>
        </li>
        <li>
            <a href="portfolio.html"><span class="top">Portfolio</span></a>
        </li>
        <li class="has-sub dropdown-wrapper from-bottom">
            <a href="#"><span class="top">Accessories</span><i class="fa fa-angle-down"></i></a>
            <div class="dropdown-content sub-holder dropdown-left narrow">
                <div class="dropdown-inner">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-12 ">
                            <div class="menu-item">
                                <div class="categories">
                                    <div class="clearfix">
                                        <div class="col-sm-12 hover-menu">
                                            <ul>
                                                <li><a href="contact-01.html">contact us</a></li>
                                                <li><a href="faq.html">faq</a></li>
                                                <li><a href="our-gallery">our gallery</a></li>
                                                <li><a href="our-services.html">our services</a></li>
                                                <li><a href="shop-grid-three">our shop</a></li>
                                                <li><a href="our-gallery.html">our gallery</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <a href="cart-default.html"><span class="top">Cart</span></a>
        </li>
        <li>
            <a href="my-account.html"><span class="top">My Account</span></a>
        </li>
        <li>
            <a href="#"><span class="top">wishlist</span></a>
        </li>
        <li>
            <a href="my-account.html"><span class="top">login</span></a>
        </li>
    </ul>
</div>
<div class="b-wrapper">
    <header id="b-header">
        <div class="b-header b-header_bg">
            <div class="container">
                <div class="b-header_topbar row clearfix">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="b-top_bar_left float-md-left text-center text-white">
                            <i class="fa fa-phone-square text-white mr-1"> </i>
                            OUR PHONE NUMBER:
                            <a href="tel:+77756334876" class="text-white ml-2">+77 (756) 334 876</a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden-sm-down">
                        <div class="b-top_bar_right clearfix">
                            <div class="b-topbar_menu float-md-right text-center">
                                <div class="b-menu_top_bar_container">
                                    <ul id="b-menu_top_bar" class="b-menu">
                                        <li><a href="my-account.html"><i class="fa fa-user mr-1"></i>My Account</a></li>
                                        <li><a href="cart-default.html">Cart</a></li>
                                        <li><a href="#">our location</a></li>
                                        <li><a href="contact-01.html">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-header b-header_main">
            <div class="container">
                <div class="clearfix row">
                    <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-12 col-xs-12 hidden-sm-down hidden-md-down">
                        <div class="b-header_nav">
                            <div class="b-menu_top_bar_container">
                                <div class="b-main_menu menu-stay-left">
                                    <ul class="categories pl-0 mb-0 list-unstyled">
                                        <!-- Mega menu -->
                                        <!-- Top level items -->
                                        <li class=" b-has_sub b-dropdown_wrapper from-bottom">
                                            <a href="indexPrev.html" class=" description ">
                                                <span class="top">Home</span><i class="fa fa-angle-down"></i></a>
                                            <!-- Sub Menu items -->
                                            <div class="b-dropdown_content sub-holder dropdown-left" style="width: 1000px;">
                                                <div class="dropdown-inner">
                                                    <div class="row">
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="indexPrev.html" class="d-block"><strong><span>1.</span> HOME DEFAULT</strong></a>
                                                                <a href="indexPrev.html"><img src="assets/images/nav-prev/prev-main-01.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-lingerie.html" class="d-block"><strong><span>2.</span>Lingerie store</strong></a>
                                                                <a href="home-lingerie.html"><img src="assets/images/nav-prev/prev-main-02.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-watch.html" class="d-block"><strong><span>3.</span> HOME Watches</strong></a>
                                                                <a href="home-watch.html"><img src="assets/images/nav-prev/prev-main-03.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-minimalist.html" class="d-block"><strong><span>4.</span> home minimalist</strong></a>
                                                                <a href="home-minimalist.html"><img src="assets/images/nav-prev/prev-main-04.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-jewellery.html" class="d-block"><strong><span>5.</span> home jewellery</strong></a>
                                                                <a href="home-jewellery.html"><img src="assets/images/nav-prev/prev-main-10.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-furniture.html" class="d-block"><strong><span>6.</span> home furniture</strong></a>
                                                                <a href="home-furniture.html"><img src="assets/images/nav-prev/prev-main-05.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-full-width.html" class="d-block"><strong><span>7.</span> home fashion full</strong></a>
                                                                <a href="home-full-width.html"><img src="assets/images/nav-prev/prev-main-09.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-fashion.html" class="d-block"><strong><span>8.</span> home fashion</strong></a>
                                                                <a href="home-fashion.html"><img src="assets/images/nav-prev/prev-main-06.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-cosmetics.html" class="d-block"><strong><span>9.</span> home cosmetics</strong></a>
                                                                <a href="home-cosmetics.html"><img src="assets/images/nav-prev/prev-main-08.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-mb-3 col-sm-3 col-xs-12">
                                                            <div class="b-menu_content_in mb-4">
                                                                <a href="home-dark.html" class="d-block"><strong><span>10.</span> home dark</strong></a>
                                                                <a href="home-dark.html"><img src="assets/images/nav-prev/prev-main-07.jpg" class="img-fluid d-block m-auto" alt=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- row -->
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Top level items -->
                                        <li class=" b-has_sub b-dropdown_wrapper from-bottom">
                                            <a href="shop-grid-three.html" class=" description ">
                                                <span class="top">Shop</span><i class="menu-tag sale">SALE</i><i class="fa fa-angle-down"></i></a>
                                            <!-- Sub Menu items -->
                                            <div class="b-dropdown_content sub-holder dropdown-left" style="width: 1000px;">
                                                <div class="dropdown-inner">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-3">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>SHOP STYLES</b></h4>
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="#">Masonry grid</a></li>
                                                                                <li><a href="shop-grid-leftbar.html">Alternative shop</a></li>
                                                                                <li><a href="shop-grid-four.html">Default style</a></li>
                                                                                <li><a href="shop-hover-02.html">Button on hover</a></li>
                                                                                <li><a href="shop-hover-03.html">Button hover alt</a></li>
                                                                                <li><a href="shop-hover-04.html">Hover info</a></li>
                                                                                <li><a href="shop-hover-05.html">Standard button</a></li>
                                                                                <li><a href="shop-hover-06.html">Quick shop products <i class="menu-tag new">NEW</i></a></li>
                                                                                <li><a href="shop-grid-list-switcher.html">Grid/List switcher <i class="menu-tag hot">HOT</i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>PRODUCT PAGES</b></h4>
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="product-single.html">Default style</a></li>
                                                                                <li><a href="product-single-alternative.html">Alternative style</a></li>
                                                                                <li><a href="product-single-variation-image.html">Variations images</a></li>
                                                                                <li><a href="product-with-slider.html">Thumbnails bottom</a></li>
                                                                                <li><a href="compact.html">Compact <i class="menu-tag new">NEW</i></a></li>
                                                                                <li><a href="sticky-details.html">Sticky details</a></li>
                                                                                <li><a href="extra-content.html">Extra content <i class="menu-tag hot">HOT</i></a></li>
                                                                                <li><a href="thumbnails-left.html">Thumbnails left</a></li>
                                                                                <li><a href="product-with-background.html">Product with background</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>PRODUCT FEATURES</b></h4>
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="360-product-viewer.html">360° product viewer</a></li>
                                                                                <li><a href="zoom-image.html">Zoom image</a></li>
                                                                                <li><a href="with-video.html">With video</a></li>
                                                                                <li><a href="large-image.html">Large Image</a></li>
                                                                                <li><a href="infanite-scroll.html">Infinit scrolling <i class="menu-tag new">NEW</i></a></li>
                                                                                <li><a href="variable-product.html">Variable Product</a></li>
                                                                                <li><a href="groped-product.html">Grouped Product</a></li>
                                                                                <li><a href="external-product.html">External Product</a></li>
                                                                                <li><a href="sidebar-right.html">Sidebar right</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>SHOP PAGES</b></h4>
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="shop-grid-two.html">2 Columns</a></li>
                                                                                <li><a href="shop-grid-three.html">3 Columns</a></li>
                                                                                <li><a href="shop-grid-four.html">4 Columns</a></li>
                                                                                <li><a href="shop-grid-six.html">6 Columns</a></li>
                                                                                <li><a href="shop-grid-leftbar.html">Sidebar Left</a></li>
                                                                                <li><a href="shop-grid-rightbar.html">Sidebar Right</a></li>
                                                                                <li><a href="shop-grid-six.html">Full width</a></li>
                                                                                <li><a href="#">Category banner</a></li>
                                                                                <li><a href="#">RTL Shop page <i class="menu-tag new">NEW</i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- row -->
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Top level items -->
                                        <li class="b-has_sub b-dropdown_wrapper from-bottom">
                                            <a href="product-single.html"><span class="top">Blog</span><i class="fa fa-angle-down"></i></a>
                                            <!-- Sub Menu items -->
                                            <div class="b-dropdown_content sub-holder b-dropdown_left glyphicon-arrow-down hidden">
                                                <div class="dropdown-inner">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="menu-item">
                                                                <!-- CATEGORIES CONTENT -->
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="blog-default.html" class="text-uppercase">Blog Default</a></li>
                                                                                <li><a href="blog-masonry.html" class="text-uppercase">Blog Masonry</a></li>
                                                                                <li><a href="blog-fullwidth.html" class="text-uppercase">Blog Fullwidth</a></li>
                                                                                <li><a href="blog-three-columns.html" class="text-uppercase">Blog 3 Column</a></li>
                                                                                <li><a href="blog-four-columns.html" class="text-uppercase">Blog 4 Column</a></li>
                                                                                <li><a href="blog-alternative.html" class="text-uppercase">Blog Alternative</a></li>
                                                                                <li><a href="blog-single.html" class="text-uppercase">Blog Single Post</a></li>
                                                                                <li><a href="#" class="text-uppercase">Blog RTL</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- row -->
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Top level items -->
                                        <li class="b-has_sub b-dropdown_wrapper from-bottom">
                                            <a href="product-single.html"><span class="top">PAGES</span><i class="fa fa-angle-down"></i></a>
                                            <!-- Sub Menu items -->
                                            <div class="b-dropdown_content sub-holder dropdown-left" style="width: 800px;">
                                                <div class="dropdown-inner">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-3 ">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>PAGES</b></h4>
                                                                <!-- CATEGORIES CONTENT -->
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="faq.html">FaQs</a></li>
                                                                                <li><a href="about-me.html">About Me</a></li>
                                                                                <li><a href="shop-grid-three.html">Our Shop</a></li>
                                                                                <li><a href="our-services.html">Our Service</a></li>
                                                                                <li><a href="#">Our Company</a></li>
                                                                                <li><a href="contact-01.html">Contact Us</a></li>
                                                                                <li><a href="our-gallery.html">Our Gallery</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 ">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>HEADER</b></h4>
                                                                <!-- CATEGORIES CONTENT -->
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="header-01.html">Header base</a></li>
                                                                                <li><a href="header-02.html">Simplified</a></li>
                                                                                <li><a href="header-03.html">With logo center</a></li>
                                                                                <li><a href="header-04.html">Categories menu <i class="menu-tag hot">HOT</i></a></li>
                                                                                <li><a href="header-05.html">Top bar menu</a></li>
                                                                                <li><a href="#">Split menu</a></li>
                                                                                <li><a href="header-07.html">Dark header</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 ">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>PORTFOLIO</b></h4>
                                                                <!-- CATEGORIES CONTENT -->
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="portfolio.html">Portfolio Default</a></li>
                                                                                <li><a href="portfolio-alternative.html">Portfolio Alternative</a></li>
                                                                                <li><a href="portfolio-text.html">Portfolio Text</a></li>
                                                                                <li><a href="portfolio-nospace.html">Portfolio Nospace</a></li>
                                                                                <li><a href="portfolio-fullwidth.html">Portfolio Fullwidth</a></li>
                                                                                <li><a href="portfolio-single.html">Single Project</a></li>
                                                                                <li><a href="portfolio-single-fullwidth.html">Single Project Full</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 ">
                                                            <div class="menu-item">
                                                                <h4 class="column-title"><b>SHOP</b></h4>
                                                                <!-- CATEGORIES CONTENT -->
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu">
                                                                            <ul>
                                                                                <li><a href="cart-default.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                                                                <li><a href="#"><i class="fa fa-credit-card"></i>  Checkout</a></li>
                                                                                <li><a href="my-account.html"><i class="fa fa-user"></i>  My Account</a></li>
                                                                                <li><a href="wishlist.html"><i class="fa fa-heart"></i>  Wishlist</a></li>
                                                                                <li><a href="track-order.html">Track Order</a></li>
                                                                                <li><a href="404.html">404</a></li>
                                                                                <li><a href="maintenance.html">Maintenance mode</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- row -->
                                                </div>
                                            </div>
                                        </li>
                                        <li class="b-has_sub b-dropdown_wrapper from-bottom">
                                            <a href="#"><span class="top">FEATURES</span><i class="fa fa-angle-down"></i></a>
                                            <!-- Sub Menu items -->
                                            <div class="b-dropdown_content sub-holder b-dropdown_left glyphicon-arrow-down hidden">
                                                <div class="dropdown-inner">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="menu-item">
                                                                <!-- CATEGORIES CONTENT -->
                                                                <div class="categories">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 hover-menu text-uppercase">
                                                                            <ul>
                                                                                <li><a href="recent-products.html">Recent Products</a>
                                                                                <li><a href="featured-products.html">Featured Products</a>
                                                                                <li><a href="single-product.html">Single Product</a>
                                                                                <li><a href="products-by-id.html">Products by ID</a>
                                                                                <li><a href="products-category.html">Products Category <i class="menu-tag new">NEW</i></a>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- row -->
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-4 col-xs-6">
                        <div class="b-logo text-sm-left text-lg-center text-xl-center">
                            <a href="indexPrev.html" class="d-inline-block"><img src="assets/images/logo.png" class="img-fluid d-block" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-mb-4 col-sm-8 col-xs-6">
                        <div class="b-header_right">
                            <div class="b-header_links hidden-sm-down">
                                <ul class="pl-0 mb-0 list-unstyled">
                                    <li>
                                        <a href="my-account.html">Login / Register</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="b-search_icon hidden-sm-down">
                                <a href="javascript:;" id="b-search_toggle" class="d-inline-block">
                                    <i class="icon-magnifier icons"></i>
                                </a>
                            </div>
                            <div class="b-wishlist_icon">
                                <a href="wishlist.html" class="d-inline-block">
                                    <i class="icon-heart icons"></i>
                                    <span>3</span>
                                </a>
                            </div>
                            <div class="b-cart_basket pr-0">
                                <a href="javascript:void(0);" id="b-mini_cart" class="d-inline-block">
                                    <i class="icon-basket icons"></i>
                                    <span class="b-cart_totals">
                              <span class="b-cart_number">3</span>
                              <span class="b-subtotal_divider">/</span>
                              <span class="b-cart_subtotal">
                                <span class="b-cart_amount amount">
                                  <span class="b-cart_currency">£</span>100.00
                                </span>
                              </span>
                            </span>
                                </a>
                            </div>
                            <div class="hidden-lg-up">
                                <i class="icon-menu icons b-nav_icon" id="b-nav_icon"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="b-page_title b-page_title_default text-center">
        <h1 class="b-entry_title"><span>My Account</span></h1>
        <div class="b-breadcrumbs">
            <a href="indexPrev.html">Home</a>
            <span>My Account</span>
        </div>
    </div>
    <section id="b-my_account">
        <div class="container b-my_account">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="b-auth_section b-auth_login">
                        <h2><i class="icon-login icons"></i> Login</h2>
                        <form action="#">
                            <div class="form-group">
                                <label for="">Username or email address <span class="b-required">*</span></label>
                                <input type="email" name="" value="" id="">
                            </div>
                            <div class="form-group">
                                <label for="">Password <span class="b-required">*</span></label>
                                <input type="password" name="" value="" id="">
                            </div>
                            <div class="form-group clearfix">
                                <label class="pull-left"><input name="" type="checkbox" id="" value=""> Remember me</label>
                                <a href="#" class="pull-right b-lost_password"><i class="icon-support icons"></i>  Lost your password?</a>
                            </div>
                            <button type="submit" class="btn" id="">Login</button>
                        </form>
                    </div>
                    <div class="b-auth_section b-auth_register" style="display: none;">
                        <h2><i class="icon-user icons"></i> Register</h2>
                        <form action="#">
                            <div class="form-group">
                                <label for="">Username<span class="b-required">*</span></label>
                                <input type="text" name="" value="" id="">
                            </div>
                            <div class="form-group">
                                <label for="">Email address <span class="b-required">*</span></label>
                                <input type="email" name="" value="" id="">
                            </div>
                            <div class="form-group">
                                <label for="">Password <span class="b-required">*</span></label>
                                <input type="password" name="" value="" id="">
                            </div>
                            <button type="submit" class="btn" id="">Register</button>
                        </form>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="b-auth_text text-center b-auth_text_register">
                        <h3>Register</h3>
                        <p>Registering for this site allows you to access your order status and history. Just fill in the fields below, and we’ll get a new account set up for you in no time. We will only ask you for information necessary to make the purchase process faster and easier.</p>
                        <a href="javascript:;" class="btn" id="b-register_but">Register</a>
                    </div>
                    <div class="b-auth_text text-center b-auth_text_login" style="display: none;">
                        <h3>Login</h3>
                        <p>Registering for this site allows you to access your order status and history. Just fill in the fields below, and we’ll get a new account set up for you in no time. We will only ask you for information necessary to make the purchase process faster and easier.</p>
                        <a href="javascript:;" class="btn" id="b-login_but">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="b-footer_container color-scheme-light hidden -sm-down">
        <div class="container b-main_footer">
            <!-- footer-main -->
            <aside class="row clearfix">
                <div class="b-footer_column col-md-12 col-sm-12">
                    <div class="b-footer_block">
                        <div class="b-footer_block_in">
                            <p class="text-center mb-0"><img src="assets/images/logo-white.png" class="d-block m-auto img-fluid" alt="" title=""></p>
                            <ul class="b-social-icons text-center">
                                <li class="b-social_facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i>Facebook</a></li>
                                <li class="b-social_twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i>Twitter</a></li>
                                <li class="b-social_google"><a href="#" target="_blank"><i class="fa fa-google-plus"></i>Google</a></li>
                                <li class="b-social_email"><a href="#" target="_blank"><i class="fa fa-envelope"></i>Email</a></li>
                                <li class="b-social_pinterest"><a href="#" target="_blank"><i class="fa fa-pinterest"></i>Pinterest</a></li>
                            </ul>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="b-footer_column col-lg-2 col-md-3 col-sm-6 mb-4">
                    <div class="b-footer_block">
                        <h5 class="b-footer_block_title">Our Stores</h5>
                        <div class="b-footer_block_in">
                            <ul class="b-footer_menu">
                                <li><a href="#">New York</a></li>
                                <li><a href="#">London SF</a></li>
                                <li><a href="#">Cockfosters BP</a></li>
                                <li><a href="#">Los Angeles</a></li>
                                <li><a href="#">Chicago</a></li>
                                <li><a href="#">Las Vegas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="b-footer_column col-lg-2 col-md-3 col-sm-6 mb-4">
                    <div class="b-footer_block">
                        <h5 class="b-footer_block_title">Information</h5>
                        <div class="b-footer_block_in">
                            <ul class="b-footer_menu">
                                <li><a href="#">About Store</a></li>
                                <li><a href="#">New Collection</a></li>
                                <li><a href="#">Woman Dress</a></li>
                                <li><a href="contact-01.html">Contact Us</a></li>
                                <li><a href="#">Latest News</a></li>
                                <li><a href="#">Our Sitemap</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="b-footer_column col-lg-2 col-md-3 col-sm-6 mb-4">
                    <div class="b-footer_block">
                        <h5 class="b-footer_block_title">Useful links</h5>
                        <div class="b-footer_block_in">
                            <ul class="b-footer_menu">
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Returns</a></li>
                                <li><a href="#">Terms &amp; Conditions</a></li>
                                <li><a href="contact-01.html">Contact Us</a></li>
                                <li><a href="#">Latest News</a></li>
                                <li><a href="#">Our Sitemap</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="b-footer_column col-lg-2 col-md-3 col-sm-6 mb-4 mb-4">
                    <div class="b-footer_block">
                        <h5 class="b-footer_block_title">Footer Menu</h5>
                        <div class="b-footer_block_in">
                            <ul class="b-footer_menu">
                                <li><a href="#">Instagram profile</a></li>
                                <li><a href="#">New Collection</a></li>
                                <li><a href="#">Woman Dress</a></li>
                                <li><a href="contact-01.html">Contact Us</a></li>
                                <li><a href="#">Latest News</a></li>
                                <li><a href="#" target="_blank">Purchase Theme</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="b-footer_column col-lg-4 col-md-12 col-sm-12 mb-4">
                    <div class="b-footer_block">
                        <h5 class="b-footer_block_title">About The Store</h5>
                        <div class="b-footer_block_in">
                            <p>STORE - worldwide fashion store since 1978. We sell over 1000+ branded products on our web-site.</p>
                            <div class="b-contact_info">
                                <i class="fa fa-location-arrow d-inline-block"></i> 451 Wall Street, USA, New York
                                <br>
                                <i class="fa fa-mobile d-inline-block"></i> Phone: (064) 332-1233
                                <br>
                            </div>
                            <br>
                            <p><img src="assets/images/payments.png" alt=""></p>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- footer-main -->
        </div>
        <!-- footer-bar -->
        <div class="b-copyrights_wrapper">
            <div class="container">
                <div class="d-footer_bar">
                    <div class="text-center">
                        <i class="fa fa-copyright"></i> 2018 Created by
                        <a href="#" class="text-white">
                            jThemes Studio
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bar -->
    </footer>
    <a href="javascript:;" id="b-scrollToTop" class="b-scrollToTop">
        <span class="basel-tooltip-label">Scroll To Top</span>Scroll To Top
    </a>
    <div class="b-search_popup">
        <form role="search" method="get" id="searchform" class="searchform  basel-ajax-search" action="#" data-thumbnail="1" data-price="1" data-count="3">
            <div>
                <label class="screen-reader-text" for="s"></label>
                <input type="text" placeholder="Search for products" value="" name="s" id="s" autocomplete="off">
                <input type="hidden" name="post_type" id="post_type" value="product">
                <button type="submit" class="b-searchsubmit" id="b-searchsubmit">Search</button>
            </div>
        </form>
        <span class="b-close_search" id="b-close_search">close</span>
    </div>
</div>

<!-- JQUERY:: JQUERY.JS -->
<script src="assets/js/jquery.min.js"></script>

<!-- JQUERY:: BOOTSTRAP.JS -->
<script src="assets/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- JQUERY:: CUSTOM.JS -->
<script src="assets/js/custom.js"></script>

</body>
</html>