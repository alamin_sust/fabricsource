package com.exportimport.fabricsource.controller;

import com.exportimport.fabricsource.dao.ContentDao;
import com.exportimport.fabricsource.dao.NewsletterDao;
import com.exportimport.fabricsource.dao.UserDao;
import com.exportimport.fabricsource.db.Newsletter;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.service.ChatService;
import com.exportimport.fabricsource.service.UserService;
import com.exportimport.fabricsource.util.ModelMapUtils;
import com.exportimport.fabricsource.util.Statics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @Autowired
    ChatService chatService;

    @Autowired
    UserDao userDao;

    @Autowired
    ContentDao contentDao;

    @Autowired
    NewsletterDao newsletterDao;

    @RequestMapping("/")
    public String login(Model model, HttpServletRequest request, HttpServletResponse response,
                        HttpSession session) {
        return "redirect:/home";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public String getHome(ModelMap map, HttpSession session, @RequestParam(name = "type", required = false) String type) {
        if(type != null) {
            session.setAttribute("user", null);
            session.setAttribute("sm", Statics.LOGOUT_SUCCESS);
        }

        User user = (User) session.getAttribute("user");

        map.put("contentList", user!=null?contentDao.findAllByCreatedBy(user.getId()): Collections.EMPTY_LIST);


        map.put("roleMap", ModelMapUtils.getRoleMap());
        map.put("allowedRecipientMap", chatService.getAllowedRecipientMap(user));
        return "home";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/home")
    public String postLoginRegister(ModelMap map, HttpSession session,
                                    @RequestParam(name = "type", required = true) String type,
                                    @RequestParam(name = "email", required = true) String email,
                                    @RequestParam(name = "password", required = true) String password,
                                    @RequestParam(name = "name", required = false) String name,
                                    @RequestParam(name = "address", required = false) String address,
                                    @RequestParam(name = "mobile", required = false) String mobile,
                                    @RequestParam(name = "roleId", required = false) Integer roleId) {
        User user = userDao.findByEmail(email);

        if (type.equals("register")) {
            if(user == null) {
                user = new User();
                user.setName(name);
                user.setAddress(address);
                user.setMobile(mobile);
                user.setEmail(email);
                user.setPassword(password);
                user.setRoleId(Statics.NAU);
                userService.insertUser(user, roleId);
                session.setAttribute("user", user);
                session.setAttribute("sm", Statics.REGISTER_SUCCESS);
            } else {
                session.setAttribute("em", Statics.REGISTER_USER_ALREADY_EXISTS);
            }
        } else if (type.equals("login")){
            if (user == null) {
                session.setAttribute("em", Statics.LOGIN_INVALID_USER_PASS);
            } else if (!user.getPassword().equals(password)) {
                session.setAttribute("em", Statics.LOGIN_PASS_MISSMATCH);
            } else {
                session.setAttribute("user", user);
                session.setAttribute("sm", Statics.LOGIN_SUCCESS);
            }
        }

        return "redirect:/home";
    }

    @RequestMapping("/newsletter")
    public String newsletter(Model model, @RequestParam(name = "email") String email,
                             HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        Newsletter newsletter = newsletterDao.findOneByEmail(email);

        if(newsletter == null) {
            newsletter = new Newsletter();
            newsletter.setEmail(email);
            newsletterDao.save(newsletter);
        }

        return "redirect:/home";
    }
}
