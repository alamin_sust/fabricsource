package com.exportimport.fabricsource.controller;

import com.exportimport.fabricsource.dao.RequestDao;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.service.RequestService;
import com.exportimport.fabricsource.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class RequestController {

    @Autowired
    UserService userService;

    @Autowired
    RequestService requestService;

    @Autowired
    RequestDao requestDao;

    @RequestMapping("/requests")
    public String getRequests(Model model, HttpServletRequest request, HttpServletResponse response,
                              HttpSession session) {

        User user = (User) session.getAttribute("user");

        if (user == null) {
            return "redirect:/home";
        }

        model.addAttribute("requestList", requestDao.findAllByMaxRequestedRoleId(user.getRoleId() + 1));

        return "requests";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/requests")
    public String postRequests(Model model, HttpServletRequest request, HttpServletResponse response,
                               HttpSession session, @RequestParam(name = "requestId") int requestId,
                               @RequestParam(name = "type") int type) {

        requestService.acceptOrReject(type, requestId, session);

        return "redirect:/requests";
    }


}
