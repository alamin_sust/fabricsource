package com.exportimport.fabricsource.controller;

import com.exportimport.fabricsource.dao.ContentDao;
import com.exportimport.fabricsource.dao.UserDao;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;

@Controller
public class ContentController {

    @Autowired
    ContentService contentService;

    @Autowired
    UserDao userDao;

    @Autowired
    ContentDao contentDao;

    @RequestMapping("/contents")
    public String getContents(ModelMap map, HttpServletRequest request, HttpServletResponse response,
                              HttpSession session) {
        User user = (User) session.getAttribute("user");

        if (user == null) {
            return "redirect:/home";
        }

        map.put("contentList", user != null ? contentDao.findAllByCreatedBy(user.getId()) : Collections.EMPTY_LIST);
        return "contents";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/contents")
    public String postContents(Model model, HttpServletRequest request, HttpServletResponse response,
                               HttpSession session,
                               @RequestParam(name = "id", required = false) Integer id,
                               @RequestParam(name = "name", required = false) String name,
                               @RequestParam(name = "link", required = false) String link,
                               @RequestParam(name = "details", required = false) String details,
                               @RequestParam(name = "type", required = false) Integer type,
                               @RequestParam(name = "changeType", required = false) String changeType) {


        if (session.getAttribute("user") == null) {
            return "redirect:/home";
        }

        if (changeType == null) {
            contentService.saveContent(name, link, details, type, session);
            return "redirect:/home";
        } else if (changeType.equals("update")) {
            contentService.updateContent(id, name, link, details, session);
            return "redirect:/contents";
        } else if (changeType.equals("delete")) {
            contentService.deleteContent(id, session);
            return "redirect:/contents";
        }
        return "redirect:/home";
    }
}
