package com.exportimport.fabricsource.controller;

import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.service.ChatService;
import com.exportimport.fabricsource.service.CookieService;
import com.exportimport.fabricsource.service.UserService;
import com.exportimport.fabricsource.util.Statics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class ChatController {

    @Autowired
    ChatService chatService;

    @Autowired
    UserService userService;

    @Autowired
    CookieService cookieService;

    @RequestMapping(value = "/getMessages", method = RequestMethod.GET)
    public @ResponseBody
    String getMessages(HttpSession session, HttpServletResponse response,
                       @CookieValue(value = "cookieId", defaultValue = "0") int cookieId) {
        if (cookieId == 0) {
            Cookie cookie = cookieService.initializeNewCookie();
            response.addCookie(cookie);
        }

        User user = (User) session.getAttribute("user");
        return chatService.getFormattedConversation(user, cookieId);
    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public @ResponseBody
    String sendMessage(@RequestParam(value = "message") String message,
                       @RequestParam(value = "recipients", required = false) String recipients,
                       @CookieValue(value = "cookieId", defaultValue = "-1") int cookieId,
                       HttpSession session) {
        User user = (User) session.getAttribute("user");
        if(user == null || user.getRoleId() != Statics.SUPPORT) {
            chatService.sendMessage(user, message, userService.getAllSupports().get(0).getId().toString(), cookieId);
        } else {
            chatService.sendMessage(user, message, recipients.replaceAll("\"", ""), cookieId);
        }
        return chatService.getFormattedConversation(user, cookieId);
    }
}
