package com.exportimport.fabricsource.util;

public class Statics {

    //ROLES
    public static final int NAU = 99;
    public static final int MANAGER = 1;
    public static final int ADMIN = 2;
    public static final int SUPPORT = 3;
    public static final int USER = 4;
    public static final int VOLUNTEER = 5;

    //CONTENT TYPES
    public static final int CONTENT_TYPE_VIDEO = 1;
    public static final int CONTENT_TYPE_BLOG = 2;

    //REQUESTS
    public static final int ACCEPT = 1;
    public static final int REJECT = 1;

    public static final String LOGOUT_SUCCESS = "Successfully Logged Out.";
    public static final String LOGIN_SUCCESS = "Successfully Logged In.";
    public static final String LOGIN_PASS_MISSMATCH = "Email and Password doesn't macth, please try again.";
    public static final String LOGIN_INVALID_USER_PASS = "Please enter valid email and password";

    public static final String REGISTER_USER_ALREADY_EXISTS = "User already exists with same username and email";
    public static final String ACCESS_DENIED = "Sorry, You don't have the access to take this action";
    public static final String REGISTER_SUCCESS = "Successfully registerd";
    public static final String ADD_SUCCESS = "Successfully added";
    public static final String EDIT_SUCCESS = "Successfully changed";
    public static final String DELETE_SUCCESS = "Successfully deleted";
    public static final String EMAIL_SUCCESS = "Email Sent!";

    public static final String CONTACT_EMAIL_ADDRESS = "alaminbbsc@gmail.com";

    public static final int ONE_HOUR = 60;
    public static final int ONE_DAY = 24 * ONE_HOUR;
}
