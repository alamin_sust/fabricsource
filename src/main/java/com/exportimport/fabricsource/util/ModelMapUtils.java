package com.exportimport.fabricsource.util;

import com.exportimport.fabricsource.db.User;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ModelMapUtils {
    public static Map<String, Integer> getRoleMap() {
        Map<String, Integer> roleMap = new LinkedHashMap<>();
        roleMap.put("NAU", Statics.NAU);
        roleMap.put("MANAGER", Statics.MANAGER);
        roleMap.put("ADMIN", Statics.ADMIN);
        roleMap.put("SUPPORT", Statics.SUPPORT);
        roleMap.put("USER", Statics.USER);
        roleMap.put("VOLUNTEER", Statics.VOLUNTEER);
        return roleMap;
    }

    public static Map<String, Integer> getAllowedRecipientMap(List<User> recipientList) {
        Map<String, Integer> recipientMap = new LinkedHashMap<>();
        for (User recipient : recipientList) {
            recipientMap.put(recipient.getEmail(), recipient.getId());
        }
        return recipientMap;
    }
}
