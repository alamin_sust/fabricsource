package com.exportimport.fabricsource.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AuthUtils {

    public static boolean isDurationLimitNotExpiredForSave(int role, Date dateTime) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(Statics.MANAGER, Integer.MAX_VALUE);
        map.put(Statics.ADMIN, Integer.MAX_VALUE);
        map.put(Statics.SUPPORT, Integer.MAX_VALUE);
        map.put(Statics.USER, Integer.MAX_VALUE);
        map.put(Statics.VOLUNTEER, Integer.MAX_VALUE);
        map.put(Statics.NAU, -1);

        Date currentDateTime = new Date();
        long minutesPassed = (currentDateTime.getTime() - dateTime.getTime()) / 60000;
        return map.get(role) >= minutesPassed;
    }

    public static boolean isDurationLimitNotExpiredForUpdate(int role, Date dateTime) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(Statics.MANAGER, Integer.MAX_VALUE);
        map.put(Statics.ADMIN, Integer.MAX_VALUE);
        map.put(Statics.SUPPORT, Integer.MAX_VALUE);
        map.put(Statics.USER, Integer.MAX_VALUE);
        map.put(Statics.VOLUNTEER, 2 * Statics.ONE_HOUR);
        map.put(Statics.NAU, -1);

        Date currentDateTime = new Date();
        long minutesPassed = (currentDateTime.getTime() - dateTime.getTime()) / 60000;
        return map.get(role) >= minutesPassed;
    }

    public static boolean isDurationLimitNotExpiredForDelete(int role, Date dateTime) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(Statics.MANAGER, Integer.MAX_VALUE);
        map.put(Statics.ADMIN, Statics.ONE_DAY);
        map.put(Statics.SUPPORT, Integer.MAX_VALUE);
        map.put(Statics.USER, 2 * Statics.ONE_HOUR);
        map.put(Statics.VOLUNTEER, 2 * Statics.ONE_HOUR);
        map.put(Statics.NAU, -1);

        Date currentDateTime = new Date();
        long minutesPassed = (currentDateTime.getTime() - dateTime.getTime()) / 60000;
        return map.get(role) >= minutesPassed;
    }
}
