package com.exportimport.fabricsource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabricsourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabricsourceApplication.class, args);
	}

}
