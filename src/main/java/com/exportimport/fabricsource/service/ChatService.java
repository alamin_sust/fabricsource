package com.exportimport.fabricsource.service;

import com.exportimport.fabricsource.dao.ChatDao;
import com.exportimport.fabricsource.dao.ChatRecipientDao;
import com.exportimport.fabricsource.dao.UserDao;
import com.exportimport.fabricsource.db.Chat;
import com.exportimport.fabricsource.db.ChatRecipient;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.util.ModelMapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ChatService {

    @Autowired
    ChatDao chatDao;

    @Autowired
    ChatRecipientDao chatRecipientDao;

    @Autowired
    UserDao userDao;

    public String getFormattedConversation(User user, int cookieId) {

        List<ChatRecipient> chatRecipientList;

        if (user != null) {
            chatRecipientList = chatRecipientDao.findAllByRecipientId(user == null ? 0 : user.getId());
        } else {
            chatRecipientList = chatRecipientDao.findAllByRecipientCookieId(cookieId);
        }

        String formattedConversation = "";
        for (ChatRecipient chatRecipient : chatRecipientList) {
            Chat chat = chatDao.findOneById(chatRecipient.getChatId());
            formattedConversation += "<b>" + chat.getSentByName() + "</b>: " + chat.getMessage() + "<br>";
        }
        return formattedConversation;
    }

    @Transactional
    public void sendMessage(User user, String message, String recipientStr, int cookieId) {
        Chat chat = new Chat();
        chat.setMessage(message.substring(1, message.length() - 1));
        chat.setSentBy(user != null ? user.getId() : 0);
        chat.setSentByName(user != null ? user.getName() : "Me");
        chat.setCookieId(cookieId);
        chatDao.save(chat);

        String[] recipientArr = recipientStr.split("-");
        Arrays.sort(recipientArr);
        List<User> recipientList = new ArrayList<>();
        if (user!=null) {
            if (Integer.parseInt(recipientArr[0]) == 0) {
                recipientList = getAllowedRecipientList(user);
            } else {
                for (String id : recipientArr) {
                    recipientList.add(userDao.findOneById(Integer.parseInt(id)));
                }
                if (user != null) {
                    recipientList.add(user);
                }
            }

            for (User recipient: recipientList) {
                ChatRecipient chatRecipient = new ChatRecipient();
                chatRecipient.setChatId(chat.getId());
                chatRecipient.setRecipientId(recipient.getId());
                chatRecipient.setRecipientCookieId(0);
                chatRecipientDao.save(chatRecipient);
            }
        } else {
            for (String id : recipientArr) {
                recipientList.add(userDao.findOneById(Integer.parseInt(id)));
            }
            for (User recipient: recipientList) {
                ChatRecipient chatRecipient = new ChatRecipient();
                chatRecipient.setChatId(chat.getId());
                chatRecipient.setRecipientId(recipient.getId());
                chatRecipient.setRecipientCookieId(0);
                chatRecipientDao.save(chatRecipient);
            }
            ChatRecipient chatRecipient = new ChatRecipient();
            chatRecipient.setChatId(chat.getId());
            chatRecipient.setRecipientId(0);
            chatRecipient.setRecipientCookieId(cookieId);
            chatRecipientDao.save(chatRecipient);
        }
    }

    public Map<String,Integer> getAllowedRecipientMap(User user) {
        return ModelMapUtils.getAllowedRecipientMap(getAllowedRecipientList(user));
    }

    private List<User> getAllowedRecipientList(User user) {
        return userDao.findAll();
    }
}
