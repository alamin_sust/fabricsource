package com.exportimport.fabricsource.service;

import com.exportimport.fabricsource.dao.RequestDao;
import com.exportimport.fabricsource.dao.UserDao;
import com.exportimport.fabricsource.db.Request;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.util.Statics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

@Service
public class RequestService {

    @Autowired
    UserDao userDao;

    @Autowired
    RequestDao requestDao;

    public void saveRequest(User user, int roleId) {
        Request request = new Request();
        request.setEmail(user.getEmail());
        request.setCurrentRole(user.getRoleId());
        request.setRequestedRole(roleId);
        requestDao.save(request);
    }

    @Transactional
    public void acceptOrReject(int type, int requestId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        Request request = requestDao.getOne(requestId);
        User requestingUser = userDao.findByEmail(request.getEmail());

        if (user.getRoleId()>=request.getRequestedRole()) {
            session.setAttribute("em", Statics.ACCESS_DENIED);
            return;
        }

        if(type == Statics.ACCEPT) {
            requestingUser.setRoleId(request.getRequestedRole());
            userDao.save(requestingUser);
            request.setAcceptedBy(user.getId());
            requestDao.save(request);
            session.setAttribute("sm", Statics.EDIT_SUCCESS);
        } else {
            request.setRejectedBy(user.getId());
            session.setAttribute("sm", Statics.EDIT_SUCCESS);
        }
    }
}
