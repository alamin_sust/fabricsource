package com.exportimport.fabricsource.service;

import com.exportimport.fabricsource.db.Content;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.util.AuthUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class AuthService {

    public boolean canSave(HttpSession session) {
        return true;
    }

    public boolean canUpdate(HttpSession session, Content content) {
        User user = (User) session.getAttribute("user");
        return AuthUtils.isDurationLimitNotExpiredForUpdate(user.getRoleId(), content.getCreatedOn());
    }

    public boolean canDelete(HttpSession session, Content content) {
        User user = (User) session.getAttribute("user");
        return AuthUtils.isDurationLimitNotExpiredForDelete(user.getRoleId(), content.getCreatedOn());
    }
}
