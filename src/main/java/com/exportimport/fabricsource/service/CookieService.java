package com.exportimport.fabricsource.service;

import com.exportimport.fabricsource.dao.CookieDao;
import com.exportimport.fabricsource.db.OurCookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;

@Service
public class CookieService {

    @Autowired
    private CookieDao cookieDao;

    public Cookie initializeNewCookie() {

        OurCookie ourCookie = new OurCookie();
        cookieDao.save(ourCookie);

        Cookie cookie = new Cookie("cookieId", Integer.toString(ourCookie.getId()));
        cookie.setMaxAge(7 * 24 * 60 * 60); // expires in 7 days
        cookie.setHttpOnly(true);

        return cookie;
    }
}
