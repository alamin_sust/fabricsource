package com.exportimport.fabricsource.service;


import com.exportimport.fabricsource.dao.UserDao;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.util.Statics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    RequestService requestService;

    public void registerUser(User user) {

    }

    public void loginUser(User user) {
    }

    @Transactional
    public void insertUser(User user, int roleId) {
        requestService.saveRequest(user, roleId);
        userDao.save(user);
    }

    public List<User> getAllSupports() {
        return userDao.findAllByRoleId(Statics.SUPPORT);
    }
}
