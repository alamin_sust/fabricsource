package com.exportimport.fabricsource.service;

import com.exportimport.fabricsource.dao.ContentDao;
import com.exportimport.fabricsource.dao.RequestDao;
import com.exportimport.fabricsource.db.Content;
import com.exportimport.fabricsource.db.User;
import com.exportimport.fabricsource.util.Statics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

@Service
public class ContentService {

    @Autowired
    ContentDao contentDao;

    @Autowired
    RequestDao requestDao;

    @Autowired
    AuthService authService;

    @Transactional
    public void saveContent(String name, String link, String details, int type, HttpSession session) {
        if (authService.canSave(session)) {
            Content content = new Content();
            content.setName(name);
            content.setDetails(details);
            content.setLink(link);
            content.setType(type);
            content.setCreatedBy(((User) session.getAttribute("user")).getId());
            contentDao.save(content);
            session.setAttribute("sm", Statics.ADD_SUCCESS);
        } else {
            session.setAttribute("em", Statics.ACCESS_DENIED);
        }
    }

    public void updateContent(int id, String name, String link, String details, HttpSession session) {
        Content content = contentDao.getOne(id);
        if (authService.canUpdate(session, content)) {
            content.setName(name);
            content.setLink(link);
            content.setDetails(details);
            contentDao.save(content);
            session.setAttribute("sm", Statics.EDIT_SUCCESS);
        } else {
            session.setAttribute("em", Statics.ACCESS_DENIED);
        }
    }

    public void deleteContent(int id, HttpSession session) {
        Content content = contentDao.getOne(id);
        if (authService.canDelete(session, content)) {
            contentDao.delete(content);
            session.setAttribute("sm", Statics.DELETE_SUCCESS);
        } else {
            session.setAttribute("em", Statics.ACCESS_DENIED);
        }
    }
}
