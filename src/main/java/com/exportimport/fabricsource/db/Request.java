package com.exportimport.fabricsource.db;

import javax.persistence.*;

/**
 * Created by md_al on 18-Jul-19.
 */
@Entity
@Table(name = "request")
public class Request {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "email")
    String email;

    @Column(name = "current_role")
    Integer currentRole;

    @Column(name = "requested_role")
    Integer requestedRole;

    @Column(name = "accepted_by")
    Integer acceptedBy;

    @Column(name = "rejected_by")
    Integer rejectedBy;

    public Request() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(Integer currentRole) {
        this.currentRole = currentRole;
    }

    public Integer getRequestedRole() {
        return requestedRole;
    }

    public void setRequestedRole(Integer requestedRole) {
        this.requestedRole = requestedRole;
    }

    public Integer getAcceptedBy() {
        return acceptedBy;
    }

    public void setAcceptedBy(Integer acceptedBy) {
        this.acceptedBy = acceptedBy;
    }

    public Integer getRejectedBy() {
        return rejectedBy;
    }

    public void setRejectedBy(Integer rejectedBy) {
        this.rejectedBy = rejectedBy;
    }
}
