package com.exportimport.fabricsource.db;

import javax.persistence.*;

/**
 * Created by md_al on 18-Jul-19.
 */
@Entity
@Table(name = "chat_recipient")
public class ChatRecipient {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "chat_id")
    Integer chatId;

    @Column(name = "recipient_id")
    Integer recipientId;

    @Column(name = "recipient_cookie_id")
    Integer recipientCookieId;

    public ChatRecipient() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChatId() {
        return chatId;
    }

    public void setChatId(Integer chatId) {
        this.chatId = chatId;
    }

    public Integer getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    public Integer getRecipientCookieId() {
        return recipientCookieId;
    }

    public void setRecipientCookieId(Integer recipientCookieId) {
        this.recipientCookieId = recipientCookieId;
    }
}
