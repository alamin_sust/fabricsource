package com.exportimport.fabricsource.db;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by md_al on 18-Jul-19.
 */
@Entity
@Table(name = "chat")
public class Chat {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "message")
    String message;

    @Column(name = "sent_on")
    Date sentOnOn;

    @Column(name = "sent_by")
    Integer sentBy;

    @Column(name = "sent_by_name")
    String sentByName;

    @Column(name = "cookie_id")
    Integer cookieId;

    public Chat() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSentOnOn() {
        return sentOnOn;
    }

    public void setSentOnOn(Date sentOnOn) {
        this.sentOnOn = sentOnOn;
    }

    public Integer getSentBy() {
        return sentBy;
    }

    public void setSentBy(Integer sentBy) {
        this.sentBy = sentBy;
    }

    public String getSentByName() {
        return sentByName;
    }

    public void setSentByName(String sentByName) {
        this.sentByName = sentByName;
    }

    public Integer getCookieId() {
        return cookieId;
    }

    public void setCookieId(Integer cookieId) {
        this.cookieId = cookieId;
    }
}
