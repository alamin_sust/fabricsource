package com.exportimport.fabricsource.db;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by md_al on 18-Jul-19.
 */
@Entity
@Table(name = "content")
public class Content {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "name")
    String name;

    @Column(name = "type")
    Integer type;

    @Column(name = "link")
    String link;

    @Column(name = "details")
    String details;

    @Column(name = "image_link")
    Integer imageLink;

    @Column(name = "created_by")
    Integer createdBy;

    @Column(name = "created_on")
    Date createdOn;

    public Content() {
    }

    public Content(String name, Integer type, String link, String details, Integer imageLink, Integer createdBy, Date createdOn) {
        this.name = name;
        this.type = type;
        this.link = link;
        this.details = details;
        this.imageLink = imageLink;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getImageLink() {
        return imageLink;
    }

    public void setImageLink(Integer imageLink) {
        this.imageLink = imageLink;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
