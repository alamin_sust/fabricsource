package com.exportimport.fabricsource.db;

import javax.persistence.*;

@Entity
@Table(name = "our_cookie")
public class OurCookie {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
