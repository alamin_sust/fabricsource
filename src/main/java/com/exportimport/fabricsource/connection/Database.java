package com.exportimport.fabricsource.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    public Connection connection = null;

    /*Azure DB*/
    /*private final String USER_NAME = "kustom@kustomsql";
    private final String PASSWORD = "+YZ=zH+r7B4[c,9t";
    private final String DB_URL = "jdbc:mysql://kustomsql.mysql.database.azure.com/cardb";*/

    /*General DB*/
    private final String USER_NAME = "root";
    private final String PASSWORD = "2011331055";
    private final String DB_URL = "jdbc:mysql://localhost:3306/fabricsource?zeroDateTimeBehavior=convertToNull&useSSL=false";

    private final String DRIVER_NAME = "com.mysql.jdbc.Driver";

    public boolean connect() throws SQLException {
        try {
            Class.forName(DRIVER_NAME);
            connection = (Connection) DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //close();
        }
        return false;
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
