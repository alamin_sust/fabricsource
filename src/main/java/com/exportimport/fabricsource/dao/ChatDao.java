package com.exportimport.fabricsource.dao;

import com.exportimport.fabricsource.db.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by md_al on 17-Jul-19.
 */
@Repository
public interface ChatDao extends JpaRepository<Chat, Integer> {
    Chat findOneById(Integer chatId);
}
