package com.exportimport.fabricsource.dao;

import com.exportimport.fabricsource.db.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 17-Jul-19.
 */
@Repository
public interface ContentDao extends JpaRepository<Content, Integer> {

    List<Content> findAllByCreatedBy(Integer user);
}
