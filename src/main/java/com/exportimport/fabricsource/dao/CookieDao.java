package com.exportimport.fabricsource.dao;

import com.exportimport.fabricsource.db.OurCookie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CookieDao extends JpaRepository <OurCookie, Integer> {

}
