package com.exportimport.fabricsource.dao;

import com.exportimport.fabricsource.db.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 17-Jul-19.
 */
@Repository
public interface RequestDao extends JpaRepository<Request, Integer> {

    @Query("select r from Request r where r.requestedRole>=:maxId and (r.acceptedBy is null and r.rejectedBy is null)")
    List<Request> findAllByMaxRequestedRoleId(int maxId);
}
