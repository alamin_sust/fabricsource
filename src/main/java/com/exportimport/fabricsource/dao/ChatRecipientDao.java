package com.exportimport.fabricsource.dao;

import com.exportimport.fabricsource.db.ChatRecipient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 17-Jul-19.
 */
@Repository
public interface ChatRecipientDao extends JpaRepository<ChatRecipient, Integer> {
    List<ChatRecipient> findAllByRecipientId(Integer id);

    List<ChatRecipient> findAllByRecipientCookieId(int i);
}
