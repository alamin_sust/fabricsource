package com.exportimport.fabricsource.dao;

import com.exportimport.fabricsource.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 17-Jul-19.
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer> {

    @Query("select u from User u where u.email=:email")
    public User findByEmail(@Param("email") String email);

    @Query("select u from User u where u.email=:email and u.password=:password")
    public User findByEmailAndPassword(@Param("email") String email,
                                       @Param("password") String password);

    User findOneById(int parseInt);

    List<User> findAllByRoleId(int roleId);
}
